#!/bin/bash

# Common Vars
CYAN='\033[0;36m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
BOLD='\033[1m'
BOLD_END='\033[21m'

#Project Vars
PROJECT_NAME='Grillschule Elisenau'
DATABASE_NAME='gs-elisenau-db'

echo "\n${BOLD}=======> Deploying ${PROJECT_NAME} <========${BOLD_END}"
echo "\n${CYAN}Upload files:${NC}"
rsync -avze ssh --chown=32767:32767 --info=progress2 web/app/uploads/ root@svenfriedemann.de:/var/lib/dokku/data/storage/gs-elisenau

echo "\n${CYAN}Deploy Application:${NC}"
git push dokku master

echo "\n${CYAN}Deploy Database:${NC}"
docker-compose exec db \
    sh -c 'mysqldump -uroot -p"$MYSQL_ROOT_PASSWORD" "${MYSQL_DATABASE}"' \
    | ssh root@svenfriedemann.de dokku mariadb:import ${DATABASE_NAME}

echo "\n${GREEN}Deployment successful${NC}"

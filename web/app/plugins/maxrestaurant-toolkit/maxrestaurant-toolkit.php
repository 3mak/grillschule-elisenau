<?php
/**
 * Plugin Name:  Maxrestaurant Toolkit
 * Plugin URI:   http://www.onistaweb.com/
 * Description:  An easy to use theme plugin to add custom features to WordPress Theme.
 * Version:      1.0
 * Author:       Onista Web
 * Author URI:   http://www.onistaweb.com/
 * Author Email: onistaweb@gmail.com
 *
 * @package    MAXRESTAURANT_Theme_Toolkit
 * @since      1.0
 * @author     Onista Web
 * @copyright  Copyright (c) 2015-2016, Onista Web
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class MAXRESTAURANT_Theme_Toolkit {

	/**
	 * PHP5 constructor method.
	 *
	 * @since  1.0
	 */
	public function __construct() {

		// Set constant path to the plugin directory.
		add_action( 'plugins_loaded', array( &$this, 'maxrestaurant_constants' ), 1 );

		// Internationalize the text strings used.
		add_action( 'plugins_loaded', array( &$this, 'maxrestaurant_i18n' ), 2 );

		// Load the plugin functions files.
		add_action( 'plugins_loaded', array( &$this, 'maxrestaurant_includes' ), 3 );

		// Loads the admin styles and scripts.
		add_action( 'admin_enqueue_scripts', array( &$this, 'maxrestaurant_admin_scripts' ) );

		// Loads the frontend styles and scripts.
		add_action( 'wp_enqueue_scripts', array( &$this, 'maxrestaurant_frontend_scripts' ) ); 
	}

	/**
	 * Defines constants used by the plugin.
	 *
	 * @since  1.0
	 */
	public function maxrestaurant_constants() {

		// Set constant path to the plugin directory.
		define( 'MAXRESTAURANT_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );

		// Set the constant path to the plugin directory URI.
		define( 'MAXRESTAURANT_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );

		// Set the constant path to the inc directory.
		define( 'MAXRESTAURANT_INC', MAXRESTAURANT_DIR . trailingslashit( 'includes' ) );

		// Set the constant path to the shortcodes directory.
		define( 'MAXRESTAURANT_SC', MAXRESTAURANT_DIR . trailingslashit( 'shortcodes' ) );

		// Set the constant path to the assets directory.
		define( 'MAXRESTAURANT_LIB', MAXRESTAURANT_URI . trailingslashit( 'lib' ) );
	}

	/**
	 * Loads the translation files.
	 *
	 * @since  0.1.0
	 */
	public function maxrestaurant_i18n() {
		load_plugin_textdomain( "maxrestaurant-toolkit", false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Loads the initial files needed by the plugin.
	 *
	 * @since  0.1.0
	 */
	public function maxrestaurant_includes() {

		// Load CPT, CMB, Widgets
		require_once( MAXRESTAURANT_INC . 'inc.php' );
		require_once( MAXRESTAURANT_SC . 'inc.php' );
	}

	/**
	 * Loads the admin styles and scripts.
	 *
	 * @since  0.1.0
	 */
	function maxrestaurant_admin_scripts() {
		
		// Loads the popup custom style.
		wp_enqueue_style( 'maxrestaurant-toolkit-admin', trailingslashit( MAXRESTAURANT_LIB ) . 'css/admin.css', array(), '1.0', 'all' );
		wp_enqueue_script( 'maxrestaurant-toolkit-admin' , trailingslashit( MAXRESTAURANT_LIB ) . 'js/admin.js', array( 'jquery' ), '1.0', false );
	}

	/**
	 * Loads the frontend styles and scripts.
	 *
	 * @since  0.1.0
	 */
	 
	function maxrestaurant_frontend_scripts() {
		/* Google Map */
//		$map_api = "";
//
//		if( function_exists('maxrestaurant_options') ) {
//			$map_api = maxrestaurant_options("map_api");
//		}
//		if( $map_api != "" ) {
//			wp_enqueue_script( 'gmap-api', 'https://maps.googleapis.com/maps/api/js?key='.$map_api.'&callback=initMap', array(), null, true );
//		}
//		else {
//			wp_enqueue_script( 'gmap-api', 'https://maps.googleapis.com/maps/api/js?v=3.exp', array(), null, true );
//		}
		wp_enqueue_style( 'maxrestaurant-toolkit', trailingslashit( MAXRESTAURANT_LIB ) . 'css/plugin.css', array(), '1.0', 'all' );
		wp_enqueue_script( 'maxrestaurant-toolkit' , trailingslashit( MAXRESTAURANT_LIB ) . 'js/plugin.js', array( 'jquery' ), '1.0', true );
	}
	
}

new MAXRESTAURANT_Theme_Toolkit;

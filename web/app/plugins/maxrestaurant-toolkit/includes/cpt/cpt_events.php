<?php
/* CPT : Events */
if ( ! function_exists('maxrestaurant_cpt_team') ) {

	add_action( 'init', 'maxrestaurant_cpt_team', 0 );

	function maxrestaurant_cpt_team() {

		$labels = array(
			'name' =>  esc_html__('Team Member', "maxrestaurant-toolkit" ),
			'singular_name' => esc_html__('Team Member', "maxrestaurant-toolkit" ),
			'add_new' => esc_html__('Add New', "maxrestaurant-toolkit" ),
			'add_new_item' => esc_html__('Add New Team Member', "maxrestaurant-toolkit" ),
			'edit_item' => esc_html__('Edit Team Member', "maxrestaurant-toolkit" ),
			'new_item' => esc_html__('New Team Member', "maxrestaurant-toolkit" ),
			'all_items' => esc_html__('All Team Member', "maxrestaurant-toolkit" ),
			'view_item' => esc_html__('View Team Member', "maxrestaurant-toolkit" ),
			'search_items' => esc_html__('Search Team Member', "maxrestaurant-toolkit" ),
			'not_found' =>  esc_html__('No Team Member found', "maxrestaurant-toolkit" ),
			'not_found_in_trash' => esc_html__('No Team Member found in Trash', "maxrestaurant-toolkit" ),
			'parent_item_colon' => '',
			'menu_name' => esc_html__('Team Member', "maxrestaurant-toolkit")
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'supports' => array( 'title','thumbnail','editor' ),
			'rewrite'  => array( 'slug' => 'events-item' ),
			'has_archive' => false, 
			'capability_type' => 'post', 
			'hierarchical' => false,
			'menu_position' => 106,
			'menu_icon' => 'dashicons-images-alt2',
		);
		register_post_type( 'maxres_team', $args );
	}
}
?>
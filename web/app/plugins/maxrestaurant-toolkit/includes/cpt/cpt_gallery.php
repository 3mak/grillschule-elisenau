<?php
/* CPT : Gallery */
if ( ! function_exists('maxrestaurant_cpt_gallery') ) {

	add_action( 'init', 'maxrestaurant_cpt_gallery', 0 );

	function maxrestaurant_cpt_gallery() {

		$labels = array(
			'name' =>  esc_html__('Gallery', "maxrestaurant-toolkit" ),
			'singular_name' => esc_html__('Gallery', "maxrestaurant-toolkit" ),
			'add_new' => esc_html__('Add New', "maxrestaurant-toolkit" ),
			'add_new_item' => esc_html__('Add New Gallery', "maxrestaurant-toolkit" ),
			'edit_item' => esc_html__('Edit Gallery', "maxrestaurant-toolkit" ),
			'new_item' => esc_html__('New Gallery', "maxrestaurant-toolkit" ),
			'all_items' => esc_html__('All Gallery', "maxrestaurant-toolkit" ),
			'view_item' => esc_html__('View Gallery', "maxrestaurant-toolkit" ),
			'search_items' => esc_html__('Search Gallery', "maxrestaurant-toolkit" ),
			'not_found' =>  esc_html__('No Gallery found', "maxrestaurant-toolkit" ),
			'not_found_in_trash' => esc_html__('No Gallery found in Trash', "maxrestaurant-toolkit" ),
			'parent_item_colon' => '',
			'menu_name' => esc_html__('Gallery', "maxrestaurant-toolkit")
		);

		$args = array(
			'labels' => $labels,
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'supports' => array( 'title','thumbnail' ),
			'rewrite'  => array( 'slug' => 'gallery-item' ),
			'has_archive' => false, 
			'capability_type' => 'post', 
			'hierarchical' => false,
			'menu_position' => 106,
			'menu_icon' => 'dashicons-images-alt2',
		);
		register_post_type( 'maxres_gallery', $args );
	}
}
/* Register Custom Taxonomy */
add_action( 'init', 'maxrestaurant_tax_gallery', 1 );
function maxrestaurant_tax_gallery() {

	$labels = array(
		'name'                       => _x( 'Gallery Categories', 'Taxonomy General Name', 'maxrestaurant-toolkit' ),
		'singular_name'              => _x( 'Gallery Categories', 'Taxonomy Singular Name', 'maxrestaurant-toolkit' ),
		'menu_name'                  => esc_html__( 'Gallery Category', 'maxrestaurant-toolkit' ),
		'all_items'                  => esc_html__( 'All Items', 'maxrestaurant-toolkit' ),
		'parent_item'                => esc_html__( 'Parent Item', 'maxrestaurant-toolkit' ),
		'parent_item_colon'          => esc_html__( 'Parent Item:', 'maxrestaurant-toolkit' ),
		'new_item_name'              => esc_html__( 'New Item Name', 'maxrestaurant-toolkit' ),
		'add_new_item'               => esc_html__( 'Add New Item', 'maxrestaurant-toolkit' ),
		'edit_item'                  => esc_html__( 'Edit Item', 'maxrestaurant-toolkit' ),
		'update_item'                => esc_html__( 'Update Item', 'maxrestaurant-toolkit' ),
		'view_item'                  => esc_html__( 'View Item', 'maxrestaurant-toolkit' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'maxrestaurant-toolkit' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove items', 'maxrestaurant-toolkit' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'maxrestaurant-toolkit' ),
		'popular_items'              => esc_html__( 'Popular Items', 'maxrestaurant-toolkit' ),
		'search_items'               => esc_html__( 'Search Items', 'maxrestaurant-toolkit' ),
		'not_found'                  => esc_html__( 'Not Found', 'maxrestaurant-toolkit' ),
		'items_list'                 => esc_html__( 'Items list', 'maxrestaurant-toolkit' ),
		'items_list_navigation'      => esc_html__( 'Items list navigation', 'maxrestaurant-toolkit' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'maxrestaurant_gallery_tax', array( 'maxres_gallery' ), $args );
}
?>
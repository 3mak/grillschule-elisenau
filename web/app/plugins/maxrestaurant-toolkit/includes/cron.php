<?php

if ( ! function_exists( 'get_current_screen' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/screen.php' );
	require_once( ABSPATH . 'wp-admin/includes/template.php' );
}


class GS_Attendee_Cron {

	/**
	 * WP_Post_List children for Attendees
	 *
	 * @since  4.6.2
	 *
	 * @var Tribe__Tickets__Attendees_Table
	 */
	public $attendees_table;

	public $email;

	public function init() {
		if ( ! wp_next_scheduled( 'gs_send_attendee_mail' ) ) {
			wp_schedule_event( time(), 'daily', 'gs_send_attendee_mail' );
		}

		add_action( 'gs_send_attendee_mail', array( $this, 'send_attendee_mail' ) );
	}

	function send_attendee_mail() {
		$error = new WP_Error();
		$email = get_option( 'admin_email' );

		$events = get_posts( array(
			'post_type' => 'tribe_events',
			'limit'     => 100
		) );

		foreach ( $events as $event ) {
			$eventStartDate = get_post_meta( $event->ID, '_EventStartDate', true );
			$eventDateTime  = new DateTime( $eventStartDate );
			$eventDateTime->setTime( 0, 0 );
			$today = new DateTime();
			$today->setTime( 0, 0 );
			$timeDifference = $eventDateTime->diff( $today );

			if ( $timeDifference->days === 10 ) {
				$this->attendees_table = new Tribe__Tickets__Attendees_Table();
				$items                 = $this->generate_filtered_list( $event->ID );
				$event                 = get_post( $event->ID );

				ob_start();
				$attendee_tpl = Tribe__Tickets__Templates::get_template_hierarchy( 'tickets/attendees-email.php', array( 'disable_view_check' => true ) );
				include $attendee_tpl;
				$content = ob_get_clean();

				add_filter( 'wp_mail_content_type', array( $this, 'set_contenttype' ) );

				if ( ! wp_mail( $email, sprintf( esc_html__( 'Attendee List for: %s', 'event-tickets' ), $event->post_title ), $content ) ) {
					$error->add( 'email-error', esc_html__( 'Error when sending the email', 'event-tickets' ), array( 'type' => 'general' ) );

					return $error;
				}

				remove_filter( 'wp_mail_content_type', array( $this, 'set_contenttype' ) );
			}
		}


	}

	/**
	 * Generates a list of attendees taking into account the Screen Options.
	 * It's used both for the Email functionality, as well as the CSV export.
	 *
	 * @param $event_id
	 *
	 * @return array
	 * @since 4.6.2
	 *
	 */
	private function generate_filtered_list( $event_id ) {
		/**
		 * Fire immediately prior to the generation of a filtered (exportable) attendee list.
		 *
		 * @param int $event_id
		 */
		do_action( 'tribe_events_tickets_generate_filtered_attendees_list', $event_id );

		if ( empty( $this->page_id ) ) {
			$this->page_id = 'tribe_events_page_tickets-attendees';
		}

		//Add in Columns or get_column_headers() returns nothing
		$filter_name = "manage_{$this->page_id}_columns";
		add_filter( $filter_name, array( $this->attendees_table, 'get_columns' ), 15 );

		$items = Tribe__Tickets__Tickets::get_event_attendees( $event_id );

		//Add Handler for Community Tickets to Prevent Notices in Exports
		if ( ! is_admin() ) {
			$columns = apply_filters( $filter_name, array() );
		} else {
			$columns = array_filter( (array) get_column_headers( get_current_screen() ) );
			$columns = array_map( 'wp_strip_all_tags', $columns );
		}

		// We dont want HTML inputs, private data or other columns that are superfluous in a CSV export
		$hidden = array_merge( get_hidden_columns( $this->page_id ), array(
			'cb',
			'meta_details',
			'primary_info',
			'provider',
			'purchaser',
			'status',
		) );

		$hidden         = array_flip( $hidden );
		$export_columns = array_diff_key( $columns, $hidden );

		// Add additional expected columns
		$export_columns['order_id']           = esc_html_x( 'Order ID', 'attendee export', 'event-tickets' );
		$export_columns['order_status_label'] = esc_html_x( 'Order Status', 'attendee export', 'event-tickets' );
		$export_columns['attendee_id']        = esc_html_x( 'Ticket #', 'attendee export', 'event-tickets' );
		$export_columns['purchaser_name']     = esc_html_x( 'Customer Name', 'attendee export', 'event-tickets' );
		$export_columns['purchaser_email']    = esc_html_x( 'Customer Email Address', 'attendee export', 'event-tickets' );

		/**
		 * Used to modify what columns should be shown on the CSV export
		 * The column name should be the Array Index and the Header is the array Value
		 *
		 * @param array Columns, associative array
		 * @param array Items to be exported
		 * @param int   Event ID
		 */
		$export_columns = apply_filters( 'tribe_events_tickets_attendees_csv_export_columns', $export_columns, $items, $event_id );

		// Add the export column headers as the first row
		$rows = array(
			array_values( $export_columns ),
		);

		foreach ( $items as $single_item ) {
			// Fresh row!
			$row = array();

			foreach ( $export_columns as $column_id => $column_name ) {
				// If additional columns have been added to the attendee list table we can obtain the
				// values by calling the table object's column_default() method - any other values
				// should simply be passed back unmodified
				$row[ $column_id ] = $this->attendees_table->column_default( $single_item, $column_id );

				// Special handling for the check_in column
				if ( 'check_in' === $column_id && 1 == $single_item[ $column_id ] ) {
					$row[ $column_id ] = esc_html__( 'Yes', 'event-tickets' );
				}

				// Special handling for new human readable id
				if ( 'attendee_id' === $column_id ) {
					if ( isset( $single_item[ $column_id ] ) ) {
						$ticket_unique_id  = get_post_meta( $single_item[ $column_id ], '_unique_id', true );
						$ticket_unique_id  = $ticket_unique_id === '' ? $single_item[ $column_id ] : $ticket_unique_id;
						$row[ $column_id ] = esc_html( $ticket_unique_id );
					}
				}

				// Handle custom columns that might have names containing HTML tags
				$row[ $column_id ] = wp_strip_all_tags( $row[ $column_id ] );
				// Remove line breaks (e.g. from multi-line text field) for valid CSV format. Double quotes necessary here.
				$row[ $column_id ] = str_replace( array( "\r", "\n" ), ' ', $row[ $column_id ] );
			}

			$rows[] = array_values( $row );
		}

		return array_filter( $rows );
	}

	/**
	 * Sets the content type for the attendees to email functionality.
	 * Allows for sending an HTML email.
	 *
	 * @param $content_type
	 *
	 * @return string
	 * @since 4.6.2
	 *
	 */
	public function set_contenttype( $content_type ) {
		return 'text/html';
	}

}

$cron = new GS_Attendee_Cron();
$cron->init();


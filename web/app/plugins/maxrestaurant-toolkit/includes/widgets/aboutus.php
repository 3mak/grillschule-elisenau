<?php
/**
 * AboutUs widget class Maxrestaurant
 *
 * @since 2.8.0
 */
class Maxrestaurant_Widget_AboutUs extends WP_Widget {

	public function __construct() {
	
		$widget_ops = array( 'classname' => 'widget_about', 'description' => esc_html__( "About Us", "maxrestaurant-toolkit" ) );
		
		parent::__construct('widget-about', esc_html__('Maxrestaurant :: About Us', "maxrestaurant-toolkit"), $widget_ops);
		
		$this->alt_option_name = 'widget_about';
	}

	public function widget( $args, $instance ) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'ABOUT US', "maxrestaurant-toolkit" );
		
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo html_entity_decode( $args['before_widget'] ); // Widget starts to print information
		
		if ( $title ) {
			echo html_entity_decode( $args['before_title'] . $title . $args['after_title'] );
		}
		
		$text = empty( $instance['text'] ) ? '' : $instance['text'];
		$social_facebook = empty( $instance['social_facebook'] ) ? '' : $instance['social_facebook'];
		$social_twitter = empty( $instance['social_twitter'] ) ? '' : $instance['social_twitter'];
		$social_linkedin = empty( $instance['social_linkedin'] ) ? '' : $instance['social_linkedin'];
		$social_pinterest = empty( $instance['social_pinterest'] ) ? '' : $instance['social_pinterest'];
		$social_instagram = empty( $instance['social_instagram'] ) ? '' : $instance['social_instagram'];
		
		echo wpautop( wp_kses( $text, maxrestaurant_striptags() ) ); 
		
		?>
		<ul class="footer-social">
			<?php 
				if( !empty( $social_facebook ) ) {
					?>
					<li>
						<a target="_blank" href="<?php echo esc_url( $social_facebook ); ?>"><i class="fa fa-facebook"></i></a>
					</li>
					<?php 
				}
				if( !empty( $social_twitter ) ) {
					?>
					<li>
						<a target="_blank" href="<?php echo esc_url( $social_twitter ); ?>"><i class="fa fa-twitter"></i></a>
					</li>
					<?php
				}
				if( !empty( $social_linkedin ) ) {
					?>
					<li>
						<a target="_blank" href="<?php echo esc_url( $social_linkedin ); ?>"><i class="fa fa-linkedin"></i></a>
					</li>
					<?php 
				}
				if( !empty( $social_pinterest ) ) {
					?>
					<li>
						<a target="_blank" href="<?php echo esc_url( $social_pinterest ); ?>"><i class="fa fa-pinterest"></i></a>
					</li>
					<?php 
				}
				if( !empty( $social_instagram ) ) {
					?>
					<li>
						<a target="_blank" href="<?php echo esc_url( $social_instagram ); ?>"><i class="fa fa-instagram"></i></a>
					</li>
					<?php 
				}
			?>
		</ul>
		<?php
		echo html_entity_decode( $args['after_widget'] );
	}
	
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$new_instance = wp_parse_args( ( array ) $new_instance, array('title' => '') );

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		$instance['social_facebook'] = ( ! empty( $new_instance['social_facebook'] ) ) ? strip_tags( $new_instance['social_facebook'] ) : '';
		$instance['social_twitter'] = ( ! empty( $new_instance['social_twitter'] ) ) ? strip_tags( $new_instance['social_twitter'] ) : '';
		$instance['social_linkedin'] = ( ! empty( $new_instance['social_linkedin'] ) ) ? strip_tags( $new_instance['social_linkedin'] ) : '';
		$instance['social_pinterest'] = ( ! empty( $new_instance['social_pinterest'] ) ) ? strip_tags( $new_instance['social_pinterest'] ) : '';
		$instance['social_instagram'] = ( ! empty( $new_instance['social_instagram'] ) ) ? strip_tags( $new_instance['social_instagram'] ) : '';
		
		return $instance;
	}
	
	public function form( $instance ) {

		$instance = wp_parse_args( ( array ) $instance, array( 'title' => '' , 'text' => '' ) );

		$title = $instance['title'];
		$text =	empty( $instance['text'] ) ? '' : $instance['text'];
		$social_facebook = empty( $instance['social_facebook'] ) ? '' : $instance['social_facebook'];
		$social_twitter = empty( $instance['social_twitter'] ) ? '' : $instance['social_twitter'];
		$social_linkedin = empty( $instance['social_linkedin'] ) ? '' : $instance['social_linkedin'];
		$social_pinterest = empty( $instance['social_pinterest'] ) ? '' : $instance['social_pinterest'];
		$social_instagram = empty( $instance['social_instagram'] ) ? '' : $instance['social_instagram'];
		
		?>
		
		<p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('title') ); ?>" name="<?php echo esc_html( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
		<p><label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php esc_html_e( 'About Content:',"maxrestaurant-toolkit" ); ?></label>
			<textarea class="widefat" rows="10" cols="20" id="<?php echo esc_html($this->get_field_id('text') ); ?>" name="<?php echo esc_html($this->get_field_name('text') ); ?>"><?php echo esc_html($text); ?></textarea>
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_facebook') ); ?>"><?php esc_html_e('Facebook:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_facebook') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_facebook') ); ?>" type="text" value="<?php echo esc_url( $social_facebook ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_twitter') ); ?>"><?php esc_html_e('Twitter:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_twitter') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_twitter') ); ?>" type="text" value="<?php echo esc_url( $social_twitter ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_linkedin') ); ?>"><?php esc_html_e('LinkedIn:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_linkedin') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_linkedin') ); ?>" type="text" value="<?php echo esc_url( $social_linkedin ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_pinterest') ); ?>"><?php esc_html_e('Pinterest:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_pinterest') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_pinterest') ); ?>" type="text" value="<?php echo esc_url( $social_pinterest ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_instagram') ); ?>"><?php esc_html_e('instagram:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_instagram') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_instagram') ); ?>" type="text" value="<?php echo esc_url( $social_instagram ); ?>" /></label></p>
		
		<?php
	}
}
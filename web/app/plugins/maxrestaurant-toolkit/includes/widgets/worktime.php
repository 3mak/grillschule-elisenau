<?php
/**
 * WorkTime widget class Maxrestaurant
 *
 * @since 2.8.0
 */
class Maxrestaurant_Widget_WorkTime extends WP_Widget {

	public function __construct() {
	
		$widget_ops = array( 'classname' => 'widget_workingtime', 'description' => esc_html__( "WORKING TIME", "maxrestaurant-toolkit" ) );
		
		parent::__construct('widget-worktime', esc_html__('Maxrestaurant :: WORKING TIME', "maxrestaurant-toolkit"), $widget_ops);
		
		$this->alt_option_name = 'widget_workingtime';
	}

	public function widget( $args, $instance ) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( 'WORKING TIME', "maxrestaurant-toolkit" );
		
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo html_entity_decode( $args['before_widget'] ); // Widget starts to print information

		if ( $title ) {
			echo html_entity_decode( $args['before_title'] . $title . $args['after_title'] );
		}
		
		$montues = empty( $instance['montues'] ) ? '' : $instance['montues'];
		$wed = empty( $instance['wed'] ) ? '' : $instance['wed'];
		$thufri = empty( $instance['thufri'] ) ? '' : $instance['thufri'];
		$satu = empty( $instance['satu'] ) ? '' : $instance['satu'];
		$sund = empty( $instance['sund'] ) ? '' : $instance['sund'];
		$holiday = empty( $instance['holiday'] ) ? '' : $instance['holiday'];
		
		?>
		<table>
			<?php
			if($montues != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Monday - Tuesday',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($montues); ?></td>
				</tr>
				<?php
			}
			if($wed != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Wednesday',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($wed); ?></td>
				</tr>
				<?php
			}
			if($thufri != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Thursday - Friday',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($thufri); ?></td>
				</tr>
				<?php
			}
			if($satu != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Saturday',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($satu); ?></td>
				</tr>
				<?php
			}
			if($sund != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Sunday',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($sund); ?></td>
				</tr>
				<?php
			}
			if($holiday != "" ) {
				?>
				<tr>
					<td><?php esc_html_e('Public Holidays',"maxrestaurant-toolkit"); ?></td>
					<td><?php esc_html_e('------ ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($holiday); ?></td>
				</tr>
				<?php
			}
			?>
		</table>
		
		<?php
		echo html_entity_decode( $args['after_widget'] );
	}
	
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$new_instance = wp_parse_args( ( array ) $new_instance, array('title' => '') );

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['montues'] = ( ! empty( $new_instance['montues'] ) ) ? strip_tags( $new_instance['montues'] ) : '';
		$instance['wed'] = ( ! empty( $new_instance['wed'] ) ) ? strip_tags( $new_instance['wed'] ) : '';
		$instance['thufri'] = ( ! empty( $new_instance['thufri'] ) ) ? strip_tags( $new_instance['thufri'] ) : '';
		$instance['satu'] = ( ! empty( $new_instance['satu'] ) ) ? strip_tags( $new_instance['satu'] ) : '';
		$instance['sund'] = ( ! empty( $new_instance['sund'] ) ) ? strip_tags( $new_instance['sund'] ) : '';
		$instance['holiday'] = ( ! empty( $new_instance['holiday'] ) ) ? strip_tags( $new_instance['holiday'] ) : '';
		
		return $instance;
	}
	
	public function form( $instance ) {

		$instance = wp_parse_args( ( array ) $instance, array( 'title' => '' , 'text' => '' ) );

		$title = $instance['title'];
		$montues =	empty( $instance['montues'] ) ? '' : $instance['montues'];
		$wed = empty( $instance['wed'] ) ? '' : $instance['wed'];
		$thufri = empty( $instance['thufri'] ) ? '' : $instance['thufri'];
		$satu = empty( $instance['satu'] ) ? '' : $instance['satu'];
		$sund = empty( $instance['sund'] ) ? '' : $instance['sund'];
		$holiday = empty( $instance['holiday'] ) ? '' : $instance['holiday'];
		
		?>
		
		<p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('title') ); ?>" name="<?php echo esc_html( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('montues') ); ?>"><?php esc_html_e('Monday - Tuesday:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('montues') ); ?>" name="<?php echo esc_html( $this->get_field_name('montues') ); ?>" type="text" value="<?php echo esc_attr( $montues ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('wed') ); ?>"><?php esc_html_e('Wednesday:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('wed') ); ?>" name="<?php echo esc_html( $this->get_field_name('wed') ); ?>" type="text" value="<?php echo esc_attr( $wed ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('thufri') ); ?>"><?php esc_html_e('Thursday - Friday:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('thufri') ); ?>" name="<?php echo esc_html( $this->get_field_name('thufri') ); ?>" type="text" value="<?php echo esc_attr( $thufri ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('satu') ); ?>"><?php esc_html_e('Saturday:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('satu') ); ?>" name="<?php echo esc_html( $this->get_field_name('satu') ); ?>" type="text" value="<?php echo esc_attr( $satu ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('sund') ); ?>"><?php esc_html_e('Sunday:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('sund') ); ?>" name="<?php echo esc_html( $this->get_field_name('sund') ); ?>" type="text" value="<?php echo esc_attr( $sund ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('holiday') ); ?>"><?php esc_html_e('Public Holidays:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('holiday') ); ?>" name="<?php echo esc_html( $this->get_field_name('holiday') ); ?>" type="text" value="<?php echo esc_attr( $holiday ); ?>" /></label></p>
		
		<?php
	}
}
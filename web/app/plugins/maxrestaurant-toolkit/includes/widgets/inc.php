<?php
/* Widget Register / UN-register */
function maxrestaurant_manage_widgets() {

	/* Recent Post */
	require_once("recent_post.php");
	
	/* About Us */
	require_once("aboutus.php");
	
	/* Working Time */
	require_once("worktime.php");
	
	/* Instagram */
	require_once("instagram.php");

	/* Special Offer */
	require_once("special_offer.php");
	
	register_widget( 'Maxrestaurant_Widget_AboutUs' );
	
	register_widget( 'Maxrestaurant_Widget_RecentPost' );
	
	register_widget( 'Maxrestaurant_Widget_WorkTime' );
	
	register_widget( 'Maxrestaurant_Widget_IntstagramFeed' );
	
	register_widget( 'Maxrestaurant_Widget_SpecialOffer' );

}
add_action( 'widgets_init', 'maxrestaurant_manage_widgets' );
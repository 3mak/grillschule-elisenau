<?php
/**
 * SpecialOffer widget class Maxrestaurant
 *
 * @since 2.8.0
 */
class Maxrestaurant_Widget_SpecialOffer extends WP_Widget {

	public function __construct() {
	
		$widget_ops = array( 'classname' => 'widget_offer', 'description' => esc_html__( "Special Offer", "maxrestaurant-toolkit" ) );
		
		parent::__construct('widget-offer', esc_html__('Maxrestaurant :: Special Offer', "maxrestaurant-toolkit"), $widget_ops);
		
		$this->alt_option_name = 'widget_offer';
	}

	public function widget( $args, $instance ) {

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : esc_html__( '', "maxrestaurant-toolkit" );
		
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo html_entity_decode( $args['before_widget'] ); // Widget starts to print information

		if ( $title ) {
			echo html_entity_decode( $args['before_title'] . $title . $args['after_title'] );
		}
		
		$offerimg = empty( $instance['offerimg'] ) ? '' : $instance['offerimg'];
		$offertxt = empty( $instance['offertxt'] ) ? '' : $instance['offertxt'];
		$rprice = empty( $instance['rprice'] ) ? '' : $instance['rprice'];
		$sprice = empty( $instance['sprice'] ) ? '' : $instance['sprice'];
		$btntxt = empty( $instance['btntxt'] ) ? '' : $instance['btntxt'];
		$btnurl = empty( $instance['btnurl'] ) ? '' : $instance['btnurl'];
		
		?>
		<div class="offer-box">
			<img src="<?php echo esc_url($offerimg); ?>" alt="Offer" />
			<div class="ofr-cnt">
				<h5><?php echo esc_attr($offertxt); ?></h5>
				<span><sup><?php esc_html_e('$',"maxrestaurant-toolkit"); ?></sup><?php echo esc_attr($sprice); ?> <del><?php echo esc_attr($rprice); ?></del></span>
				<a href="<?php echo esc_url($btnurl); ?>" title="<?php echo esc_attr($btntxt); ?>"><?php echo esc_attr($btntxt); ?></a>
			</div>
		</div>
		<?php
		echo html_entity_decode( $args['after_widget'] );
	}
	
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$new_instance = wp_parse_args( ( array ) $new_instance, array('title' => '') );

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['offerimg'] = ( ! empty( $new_instance['offerimg'] ) ) ? strip_tags( $new_instance['offerimg'] ) : '';
		$instance['offertxt'] = ( ! empty( $new_instance['offertxt'] ) ) ? strip_tags( $new_instance['offertxt'] ) : '';
		$instance['rprice'] = ( ! empty( $new_instance['rprice'] ) ) ? strip_tags( $new_instance['rprice'] ) : '';
		$instance['sprice'] = ( ! empty( $new_instance['sprice'] ) ) ? strip_tags( $new_instance['sprice'] ) : '';
		$instance['btntxt'] = ( ! empty( $new_instance['btntxt'] ) ) ? strip_tags( $new_instance['btntxt'] ) : '';
		$instance['btnurl'] = ( ! empty( $new_instance['btnurl'] ) ) ? strip_tags( $new_instance['btnurl'] ) : '';
		
		return $instance;
	}
	
	public function form( $instance ) {

		$instance = wp_parse_args( ( array ) $instance, array( 'title' => '' , 'text' => '' ) );

		$title = $instance['title'];
		$offerimg =	empty( $instance['offerimg'] ) ? '' : $instance['offerimg'];
		$offertxt =	empty( $instance['offertxt'] ) ? '' : $instance['offertxt'];
		$rprice =	empty( $instance['rprice'] ) ? '' : $instance['rprice'];
		$sprice =	empty( $instance['sprice'] ) ? '' : $instance['sprice'];
		$btntxt =	empty( $instance['btntxt'] ) ? '' : $instance['btntxt'];
		$btnurl =	empty( $instance['btnurl'] ) ? '' : $instance['btnurl'];
		
		
		?>
		
		<p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('title') ); ?>" name="<?php echo esc_html( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
		<p>
			<label for="<?php echo esc_url( $this->get_field_id('offerimg') ); ?>"><?php esc_html_e('Image', "maxrestaurant-toolkit"); ?></label>
			<input type="text" class="widefat custom_media_url" name="<?php echo esc_attr( $this->get_field_name('offerimg') ); ?>" id="<?php echo esc_attr( $this->get_field_name('offerimg') ); ?>" value="<?php echo esc_url( $instance['offerimg'] ); ?>">
			<input type="button" value="<?php esc_html_e( 'Upload Image', "maxrestaurant-toolkit" ); ?>" class="button custom_media_upload" id="custom_image_uploader"/>
			<?php
			$offerimg != '';
            if ( $offerimg != '' ) {
				?>
				<div class="custom_media_image">
					<img class="custom_media_image" src="<?php echo esc_url($offerimg); ?>" alt="">
				</div>
				<?php
			}
			?>
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('offertxt') ); ?>"><?php esc_html_e('Offer Text:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('offertxt') ); ?>" name="<?php echo esc_html( $this->get_field_name('offertxt') ); ?>" type="text" value="<?php echo esc_attr( $offertxt ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('rprice') ); ?>"><?php esc_html_e('Real Price:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('rprice') ); ?>" name="<?php echo esc_html( $this->get_field_name('rprice') ); ?>" type="text" value="<?php echo esc_attr( $rprice ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('sprice') ); ?>"><?php esc_html_e('Discount Price:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('sprice') ); ?>" name="<?php echo esc_html( $this->get_field_name('sprice') ); ?>" type="text" value="<?php echo esc_attr( $sprice ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('btntxt') ); ?>"><?php esc_html_e('Button Text:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('btntxt') ); ?>" name="<?php echo esc_html( $this->get_field_name('btntxt') ); ?>" type="text" value="<?php echo esc_attr( $btntxt ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('btnurl') ); ?>"><?php esc_html_e('Button URL:', "maxrestaurant-toolkit" ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('btnurl') ); ?>" name="<?php echo esc_html( $this->get_field_name('btnurl') ); ?>" type="text" value="<?php echo esc_attr( $btnurl ); ?>" /></label></p>
		
		<?php
	}
}
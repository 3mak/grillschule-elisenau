<?php
/* Redux Options */
if( !function_exists('maxrestaurant_options') ) :

	function maxrestaurant_options( $option, $arr = null ) {
		global $maxrestaurant_option;

		if( $arr ) {

			if( isset( $maxrestaurant_option[$option][$arr] ) ) {
				return $maxrestaurant_option[$option][$arr];
			}
		}
		else {
			if( isset( $maxrestaurant_option[$option] ) ) {
				return $maxrestaurant_option[$option];
			}
		}
	}

endif;

if( !function_exists('maxrestaurant_booknow') ) :

	function maxrestaurant_booknow() {
		?>
		<!-- Modal -->
		<div class="modal fade menu_popupform" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><?php echo esc_html_e('&times;',"maxrestaurant"); ?></span></button>
						<?php if( maxrestaurant_options("opt_ftitle") != "" ) { ?><h4 class="modal-title" id="myModalLabel"><?php echo esc_attr( maxrestaurant_options("opt_ftitle") ); ?></h4><?php } ?>
					</div>
					<div class="modal-body">
						<?php echo do_shortcode( maxrestaurant_options("opt_booknow") ); ?>
					</div>					
				</div>
			</div>
		</div>
		<?php
	}
endif;

if( !function_exists('maxrestaurant_copyright') ) :

	function maxrestaurant_copyright() {
		?>
		<!-- Bottom Footer -->
		<div class="container-fluid bottom-footer">
			<?php echo do_shortcode( wpautop( wp_kses( maxrestaurant_options("opt_footer_copyright"), maxrestaurant_allowhtmltags() ) ) ); ?>
		</div><!-- Bottom Footer /- -->
		<?php
	}

endif;

/* WPAUTOP Allow Html Tags */
if( ! function_exists('maxrestaurant_striptags') ) {
	
	function maxrestaurant_striptags() {

		$maxrestaurant_allowed_tags = array(
			'a' => array( 'class' => array(),'href'  => array(), 'rel'   => array(), 'title' => array(), ),
			'abbr' => array( 'title' => array(),),
			'b' => array(),
			'blockquote' => array( 'cite'  => array(),),
			'cite' => array('title' => array(),),
			'code' => array(),
			'del' => array( 'datetime' => array(), 'title' => array(),
			),
			'dd' => array(),
			'dl' => array(),
			'dt' => array(),
			'em' => array(),
			'h1' => array(),
			'h2' => array(),
			'h3' => array(),
			'h4' => array(),
			'h5' => array(),
			'h6' => array(),
			'i' => array(),
			'img' => array( 'alt'    => array(), 'class'  => array(), 'height' => array(),'src'    => array(),'width'  => array(),),
			'li' => array('class' => array(),),
			'ol' => array( 'class' => array(),),
			'p' => array('class' => array(),),
			'q' => array( 'cite' => array(), 'title' => array(), ),
			'span' => array( 'class' => array(), 'title' => array(), 'style' => array(),),
			'strong' => array(),
			'ul' => array( 'class' => array(), ),
		);
		
		return $maxrestaurant_allowed_tags;
	}
}
?>
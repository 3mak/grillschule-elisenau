<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
	return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "maxrestaurant_option";

// This line is only for altering the demo. Can be easily removed.
$opt_name = apply_filters( 'maxrestaurant_option/opt_name', $opt_name );

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
	// TYPICAL -> Change these values as you need/desire
	'opt_name'             => $opt_name,
	// This is where your data is stored in the database and also becomes your global variable name.
	'display_name'         => $theme->get( 'Name' ),
	// Name that appears at the top of your panel
	'display_version'      => $theme->get( 'Version' ),
	// Version that appears at the top of your panel
	'menu_type'            => 'menu',
	//Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
	'allow_sub_menu'       => true,
	// Show the sections below the admin menu item or not
	'menu_title'           => esc_html__( 'Maxrestaurant Options', "maxrestaurant-toolkit" ),
	'page_title'           => esc_html__( 'Maxrestaurant Options', "maxrestaurant-toolkit" ),
	// You will need to generate a Google API key to use this feature.
	// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
	'google_api_key'       => '',
	// Set it you want google fonts to update weekly. A google_api_key value is required.
	'google_update_weekly' => false,
	// Must be defined to add google fonts to the typography module
	'async_typography'     => true,
	// Use a asynchronous font on the front end or font string
	//'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
	'admin_bar'            => true,
	// Show the panel pages on the admin bar
	'admin_bar_icon'       => 'dashicons-portfolio',
	// Choose an icon for the admin bar menu
	'admin_bar_priority'   => 50,
	// Choose an priority for the admin bar menu
	'global_variable'      => '',
	// Set a different name for your global variable other than the opt_name
	'dev_mode'             => false,
	// Show the time the page took to load, etc
	'update_notice'        => true,
	// If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
	'customizer'           => true,
	// Enable basic customizer support
	//'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
	//'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

	// OPTIONAL -> Give you extra features
	'page_priority'        => null,
	// Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	'page_parent'          => 'themes.php',
	// For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	'page_permissions'     => 'manage_options',
	// Permissions needed to access the options panel.
	'menu_icon'            => '',
	// Specify a custom URL to an icon
	'last_tab'             => '',
	// Force your panel to always open to a specific tab (by id)
	'page_icon'            => 'icon-themes',
	// Icon displayed in the admin panel next to your menu_title
	'page_slug'            => '',
	// Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
	'save_defaults'        => true,
	// On load save the defaults to DB before user clicks save or not
	'default_show'         => false,
	// If true, shows the default value next to each field that is not the default value.
	'default_mark'         => '',
	// What to print by the field's title if the value shown is default. Suggested: *
	'show_import_export'   => true,
	// Shows the Import/Export panel when not used as a field.

	// CAREFUL -> These options are for advanced use only
	'transient_time'       => 60 * MINUTE_IN_SECONDS,
	'output'               => true,
	// Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	'output_tag'           => true,
	// Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	// 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

	// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	'database'             => '',
	// possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	'use_cdn'              => true,
	// If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

	// HINTS
	'hints'                => array(
		'icon'          => 'el el-question-sign',
		'icon_position' => 'right',
		'icon_color'    => 'lightgray',
		'icon_size'     => 'normal',
		'tip_style'     => array(
			'color'   => 'red',
			'shadow'  => true,
			'rounded' => false,
			'style'   => '',
		),
		'tip_position'  => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect'    => array(
			'show' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'mouseover',
			),
			'hide' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'click mouseleave',
			),
		),
	)
);

Redux::setArgs( $opt_name, $args );

// If Redux is running as a plugin, this will remove the demo notice and links
add_action( 'redux/loaded', 'maxrestaurant_remove_demo' );

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'General Settings', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-cogs',
	'id'         => 'general_settings',
	'fields'     => array(
		array(
			'id'       => 'info_siteloader',
			'type'     => 'info',
			'title'    => esc_html__( 'Site Loader', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'       => 'opt_siteloader',
			'type'     => 'switch',
			'title'    => esc_html__( 'Site Loader', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
		array(
			'id'       => 'info_rtl',
			'type'     => 'info',
			'title'    => esc_html__( 'RTL Setting', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'       => 'opt_rtl_switch',
			'type'     => 'switch',
			'title'    => esc_html__( 'RTL', 'maxrestaurant-toolkit' ),
			'default'  => "0",
			'on'       => 'On',
			'off'      => 'Off',
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Page Header', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-credit-card-alt',
	'id'         => 'page_header',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'opt_pageheader_img',
			'type'     => 'media',
			'url'      => true,
			'title'          => esc_html__( 'Page Header Image( Default )', 'maxrestaurant-toolkit' ),
			'default'  => array( 'url' => esc_url( MAXRESTAURANT_LIB )  . 'images/page-banner.jpg' ),
		),
		array(
			'id'       => 'opt_pageheader_color',
			'type'     => 'color',
			'url'      => true,
			'title'          => esc_html__( 'Title Text Color', 'maxrestaurant-toolkit' ),
			'output'   => array( '.page-banner h3'),
		),
		array(
			'id'       => 'opt_pageheadersubtxt_color',
			'type'     => 'color',
			'url'      => true,
			'title'          => esc_html__( 'Sub Title Text Color', 'maxrestaurant-toolkit' ),
			'output'   => array( '.page-banner h3 span'),
		),
		array(
			'id'       => 'opt_pageheaderbg_overlay',
			'type'     => 'color_rgba',
			'title'   => esc_html__( 'Background Overlay', "maxrestaurant-toolkit" ),
			'subtitle' => esc_html__( 'Pick a color.', "maxrestaurant-toolkit" ),
			'options' => array(
				'show_alpha_only'    => true
			),
			'output'   => array( "background-color" => ".page-banner.custombg_overlay:before" ),
		),
		array(
			'id'        => 'opt_pageheader_height',
			'type'      => 'slider',
			'title'     => esc_html__('Page Header Height', 'maxrestaurant-toolkit'),
			'subtitle'       => esc_html__( 'Allow your users to choose minimum height of page header', 'maxrestaurant-toolkit' ),
			"default"   => 320,
			"min"       => 320,
			"step"      => 1,
			"max"       => 500,
			'display_value' => 'label'
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html( 'Social Icons', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-share-alt',
	'id'         => 'social_icons',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'info_social_icon',
			'type'     => 'info',
			'title'          => esc_html__( 'Social Icons', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'             => 'opt_social_icons',
			'type'           => 'ow_repeater',
			'textOne'        => true,
			'image'          => false,
			'title'          => esc_html__( 'Social Icon Entries', 'maxrestaurant-toolkit' ),
			'subtitle'       => __( '<u>Here you can use css class like following :</u><br><br>- Elegant Icons( "<b>social_facebook</b>" )<br>- Stroke Gap( "<b>icon icon-Like</b>" )<br>- Font Awesome( "<b>fa fa-facebook</b>" )', 'maxrestaurant-toolkit' ),
			'placeholder'    => array(
				'textOne'  => "Font Icon CSS Class",
			)
		),
	),
));

/* Google Map */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Google Map', "maxrestaurant-toolkit" ),
	'icon' => 'fa fa-map',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'=>'map_api',
			'type' => 'text',
			'title' => esc_html__( 'API Key', "maxrestaurant-toolkit" ),
			'desc' => wp_kses( __( '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">Get Api Key</a>', "maxrestaurant-toolkit" ), array( 'a' => array( 'target' => array(), 'href' => array() ) ) ),
		),
		/* Fields /- */
	),
) );

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Layout Settings', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-desktop',
	'id'         => 'layout_settings',
	'fields'     => array(
		
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Body Layout', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-desktop',
	'id'         => 'body_layout',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'info_layout_body',
			'type'     => 'info',
			'title'    => esc_html__( 'Body Layout', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'       => 'layout_body',
			'type'     => 'image_select',
			'icon'     => 'fa fa-tasks',
			'title'    => esc_html__( 'Body Layout', "maxrestaurant-toolkit" ),
			'options'  => array(
				'fixed' => array(
					'alt' => 'Boxed',
					'img' => esc_url( MAXRESTAURANT_LIB ) . 'images/layout/boxed.png'
				),
				'fluid' => array(
					'alt' => 'Full',
					'img' => esc_url( MAXRESTAURANT_LIB ) . 'images/layout/full.png'
				),
			),			
			'default'  => 'fixed'
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Sidebar Settings', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-columns',
	'id'         => 'sidebar_layout',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'info_sidebar_layout',
			'type'     => 'info',
			'title'    => esc_html__( 'Sidebar Layout', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'       => 'layout_sidebar',
			'type'     => 'image_select',
			'icon'     => 'fa fa-tasks',
			'title'    => esc_html__( 'Sidebar Settings', "maxrestaurant-toolkit" ),
			'options'  => array(
				'right_sidebar' => array(
					'alt' => 'Right Sidebar',
					'img' => esc_url( MAXRESTAURANT_LIB ) . 'images/layout/right_side.png'
				),
				'left_sidebar' => array(
					'alt' => 'Left Sidebar',
					'img' => esc_url( MAXRESTAURANT_LIB ) . 'images/layout/left_side.png'
				),
				'no_sidebar' => array(
					'alt' => 'No Sidebar',
					'img' => esc_url( MAXRESTAURANT_LIB ) . 'images/layout/no_side.png'
				),
			),			
			'default'  => 'right_sidebar'
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Header/Footer', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-archive',
	'id'         => 'site_headerfooter',
	'fields'     => array(
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Header', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-credit-card',
	'subsection' => true,
	'id'         => 'site_header',
	'fields'     => array(
		
		array(
			'id'       => 'info_sitelogo',
			'type'     => 'info',
			'notice' => true,
			'style' => 'critical',
			'icon' => 'fa fa-cube',
			'title'    => esc_html__( 'Site Logo', 'maxrestaurant-toolkit' ),
			'subtitle' => esc_html__( 'Choose Logo Type', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'       => 'opt_logotype',
			'type'     => 'select',
			'title'    => esc_html__( 'Logo Type', "maxrestaurant-toolkit" ),
			'options'  => array(
				'1' => 'Site Title',
				'2' => 'Image',
				'3' => 'Custom Text',
			),
			'default'  => '2',
		),
		array(
			'id'             => 'opt_logo_size',
			'type'           => 'dimensions',
			'units'          => array( 'px' ),    // You can specify a unit value. Possible: px, em, %
			'units_extended' => 'true',  // Allow users to select any type of unit
			'title'          => esc_html__( 'Logo (Width/Height) Option', "maxrestaurant-toolkit" ),
			'required' => array( 'opt_logotype', '=', '2' ),
		),
		array(
			'id'=>'opt_logoimg',
			'type' => 'media',
			'title' => esc_html__('Logo Upload', "maxrestaurant-toolkit" ),
			'required' => array( 'opt_logotype', '=', '2' ),
			'default'  => array( 'url' => esc_url( MAXRESTAURANT_LIB ) . 'images/logo2.png' ),
		),
		array(
			'id'=>'opt_customtxt',
			'type' => 'text',
			'title' => esc_html__('Custom Text', "maxrestaurant-toolkit" ),
			'required' => array( 'opt_logotype', '=', '3' ),
			'default'  => "Maxrestaurant"
		),
		
		array(
			'id'       => 'opt_top_header',
			'type'     => 'switch',
			'title'    => esc_html__( 'Top Header Details', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
		array(
			'id'       => 'info_contact_details',
			'type'     => 'info',
			'notice' => true,
			'style' => 'critical',
			'icon' => 'fa fa-phone',
			'title'    => esc_html__( 'Contact Details', 'maxrestaurant-toolkit' ),
			'subtitle' => esc_html__( 'Contact Details for Header', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'=>'opt_resno',
			'type' => 'text',
			'title' => esc_html__('Reservation Number', "maxrestaurant-toolkit" ),
			'default'  => "(+1) 123 4567 896",
		),
		
		array(
			'id'=>'opt_ftitle',
			'type' => 'text',
			'title' => esc_html__('Title Text', "maxrestaurant-toolkit" ),
			'default'  => "Title Text",
		),
		array(
			'id'=>'opt_booknow',
			'type' => 'text',
			'title' => esc_html__('Book Now Form', "maxrestaurant-toolkit" ),
			'subtitle' => esc_html__( 'Example For Add Contact Form Shortcode : [contact-form-7 id="339" title="Reservation Form 1"] ', "maxrestaurant-toolkit" ),
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Footer', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-window-maximize',
	'id'         => 'site_footer',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		
		array(
			'id'       => 'info_ftr_widget',
			'type'     => 'info',
			'notice' => true,
			'style' => 'critical',
			'icon' => 'fa fa-cube',
			'title'    => esc_html__( 'Footer Widget Column', 'maxrestaurant-toolkit' ),
		),
				
		array(
			'id'       => 'opt_footer_widcol',
			'type'     => 'select',
			'title'    => esc_html__('Select Option', 'maxrestaurant-toolkit'), 
			'subtitle' => esc_html__('Display Item In Column Layout', 'maxrestaurant-toolkit'),
			'options'  => array(
				'1col' => '1 Column',
				'2col' => '2 Column',
				'3col' => '3 Column',
				'4col' => '4 Column',
			),
			'default'  => '4col',
		),
		/* Fields /- */
		array(
			'id'       => 'opt_footer_copyright',
			'type'     => 'editor',
			'title'    => esc_html__( 'Copyright Text', "maxrestaurant-toolkit" ),
			'subtitle' => esc_html__( 'Use any of the features of WordPress editor inside your panel!', "maxrestaurant-toolkit" ),
			'default'  => '&copy; Copyrights [year] - All Rights Reserved',
			 'args'   => array(
				'teeny'            => true,
				'textarea_rows'    => 10
			)
		),
	),
));

Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Other Pages', "maxrestaurant-toolkit" ),
	'icon'         => 'el el-file',
	'id'         => 'other_pages',
	'fields'     => array(),
));

/* Woocommerce Product Display Column */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Woocommerce Product Display', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-shopping-cart',
	'id'         => 'wc_display',
	'subsection' => true,
	'fields'     => array(
	
		/* Fields */
		array(
			'id'       => 'opt_wc_column',
			'type'     => 'select',
			'title'    => esc_html__('Select Option', 'maxrestaurant-toolkit'), 
			'subtitle' => esc_html__('Display Item In Column Layout', 'maxrestaurant-toolkit'),
			'options'  => array(
				'1' => '1 Column',
				'2' => '2 Column',
				'3' => '3 Column',
				'4' => '4 Column',
			),
			'default'  => '3',
		)
		/* Fields /- */
	),
));

/* Blog Single Post */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Blog Single Post Options', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-commenting-o',
	'id'         => 'blog_post',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'opt_post_tags',
			'type'     => 'switch',
			'title'    => esc_html__( 'Tags', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
		array(
			'id'       => 'opt_post_category',
			'type'     => 'switch',
			'title'    => esc_html__( 'Categories', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
		array(
			'id'       => 'opt_post_author',
			'type'     => 'switch',
			'title'    => esc_html__( 'Author Details', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
		array(
			'id'       => 'opt_post_Share',
			'type'     => 'switch',
			'title'    => esc_html__( 'Social Share', 'maxrestaurant-toolkit' ),
			'default'  => "1",
			'on'       => 'On',
			'off'      => 'Off',
		),
	),
));

/* Search Page */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Search Page', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-search',
	'id'         => 'page_search',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'opt_search_bg',
			'type'     => 'media',
			'url'      => true,
			'title'          => esc_html__( 'Search Page Header Background Image( Default )', 'maxrestaurant-toolkit' ),
			'default'  => array( 'url' => esc_url( MAXRESTAURANT_LIB ) . 'images/page-banner.jpg' ),
		),
	),
));

/* 404 Page */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( '404 Page', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-exclamation-triangle',
	'id'         => 'page_error',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'opt_error_title',
			'type'     => 'text',
			'title'    => esc_html__( 'Title', "maxrestaurant-toolkit" ),
			'default'  => '404',
		),
		array(
			'id'       => 'opt_error_subtitle',
			'type'     => 'text',
			'title'    => esc_html__( 'Sub Title', "maxrestaurant" ),
			'default'  => 'Oops Error ! This Page Not Found',
		),
		array(
			'id'       => 'opt_error_img',
			'type'     => 'media',
			'url'      => true,
			'title'          => esc_html__( '404 Image( Default )', 'maxrestaurant-toolkit' ),
			'default'  => array( 'url' => esc_url( MAXRESTAURANT_LIB ) . 'images/404.jpg' ),
		),
		
		array(
			'id'       => 'opt_404_bg',
			'type'     => 'media',
			'url'      => true,
			'title'          => esc_html__( '404 Page Header Background Image( Default )', 'maxrestaurant-toolkit' ),
			'default'  => array( 'url' => esc_url( MAXRESTAURANT_LIB ) . 'images/page-banner.jpg' ),
		),
	),
));

/* Admin Login */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Admin Login Page', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-lock',
	'id'         => 'page_admin',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'opt_adminlogo',
			'type'     => 'media',
			'title'    => esc_html__( 'Logo Image', "maxrestaurant-toolkit" ),
		),
		array(
			'id'       => 'opt_adminbg_color',
			'type'     => 'color',
			'title'    => esc_html__( 'Background Color', "maxrestaurant-toolkit" ),
		),
		array(
			'id'       => 'opt_adminbg_img',
			'type'     => 'media',
			'title'    => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
		),
		array(
			'id'       => 'opt_admincolor',
			'type'     => 'color',
			'title'    => esc_html__( 'Text Color', "maxrestaurant-toolkit" ),
		),
	),
));

/* Shortcodes */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Shortcodes', "maxrestaurant-toolkit" ),
	'id'    => 'shortcode_options',
	'icon'  => 'el el-credit-card',
	'subsection' => false,
	'fields'     => array(
	)
) );

/* Service  */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Services', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-server',
	'id'         => 'service',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_service',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Service Item', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Contact Number', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Counter  */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Counter/Skill', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-trophy',
	'id'         => 'counter',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_counter',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Counter/Skill', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Counter Value', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Testimonial */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Testimonial', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-quote-left',
	'id'         => 'testimonail',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_testimonial',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Testimonial', "maxrestaurant-toolkit" ),
			'image'    => false,
			'url'    => false,
			'description'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'description'    => esc_html__('Description Text', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Menu Card */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Menu Card', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-bars',
	'id'         => 'menucard',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_menucard',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Menu Card Item', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'textTwo'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Menu Item List', "maxrestaurant-toolkit" ),
				'textTwo'    => esc_html__('Item Price', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Menu Dinner List */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Menu Dinner List', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-bars',
	'id'         => 'menudinner',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_menudinner',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Menu Dinner Item', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'textTwo'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Menu Item List', "maxrestaurant-toolkit" ),
				'textTwo'    => esc_html__('Item Price', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Menu BreakFast List */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Menu BreakFast List', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-bars',
	'id'         => 'menubreakfast',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_menubreakfast',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Menu BreakFast List', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'textTwo'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant" ),
				'textOne'    => esc_html__('Menu Item List', "maxrestaurant" ),
				'textTwo'    => esc_html__('Item Price', "maxrestaurant" ),
			),
		),
		/* Fields /- */
	)
));

/* Menu WarmDishes List */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Menu WarmDishes List', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-bars',
	'id'         => 'menuwarmdish',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_menuwarmdish',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Menu WarmDishes List', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'textTwo'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Menu Item List', "maxrestaurant-toolkit" ),
				'textTwo'    => esc_html__('Item Price', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Team */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Our Team', "maxrestaurant-toolkit"),
	'icon'         => 'fa fa-user',
	'id'         => 'team',
	'subsection' => true,
	'fields'     => array(
		/* Fields */
		array(
			'id'       => 'opt_team',
			'type'     => 'ow_repeater', 
			'title'    => esc_html__('Our Team', "maxrestaurant-toolkit" ),
			'image'    => true,
			'url'    => false,
			'textOne'    => true,
			'textTwo'    => true,
			'textThree'    => true,
			'textFour'    => true,
			'textFive'    => true,
			'placeholder'    => array(
				'title'    => esc_html__('Title', "maxrestaurant-toolkit" ),
				'textOne'    => esc_html__('Designation', "maxrestaurant-toolkit" ),
				'textTwo'    => esc_html__('Facebook URL', "maxrestaurant-toolkit" ),
				'textThree'    => esc_html__('Twitter URL', "maxrestaurant-toolkit" ),
				'textFour'    => esc_html__('Google Plus', "maxrestaurant-toolkit" ),
				'textFive'    => esc_html__('Instagram URL', "maxrestaurant-toolkit" ),
			),
		),
		/* Fields /- */
	)
));

/* Custom Css */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Custom CSS', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-code',
	'id'         => 'custom_css',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'        => 'custom_css_desktop',
			'type'      => 'ace_editor',
			'title'     => esc_html__('Custom CSS for 992 to Larger Screen Resolution (iPad Landscape & Desktop)', 'maxrestaurant-toolkit'),
			'subtitle'  => esc_html__('Paste your CSS code here.', 'maxrestaurant-toolkit'),
			'mode'      => 'css',
			'theme'     => 'monokai',
		),
		array(
			'id'        => 'custom_css_tablet',
			'type'      => 'ace_editor',
			'title'     => esc_html__('Custom CSS for 768px to 991px Screen Resolution (iPad Portrait)', 'maxrestaurant-toolkit'),
			'subtitle'  => esc_html__('Paste your CSS code here.', 'maxrestaurant-toolkit'),
			'mode'      => 'css',
			'theme'     => 'monokai',
		),
		array(
			'id'        => 'custom_css_mobile',
			'type'      => 'ace_editor',
			'icon'     => "fa fa-tasks",
			'title'     => esc_html__('Custom CSS for 767px to Lower Screen Resolution (iPhone Landscape)', 'maxrestaurant-toolkit'),
			'subtitle'  => esc_html__('Paste your CSS code here.', 'maxrestaurant-toolkit'),
			'mode'      => 'css',
			'theme'     => 'monokai',
		),
	),
));

/* Typography Css */
Redux::setSection( $opt_name, array(
	'title'      => esc_html__( 'Typography', "maxrestaurant-toolkit" ),
	'icon'         => 'fa fa-text-height ',
	'id'         => 'site_typography',
	'subsection' => false,
	'fields'     => array(
		array(
			'id'       => 'info_body_font',
			'type'     => 'info',
			'title'    => esc_html__( 'Body Font Settings', 'maxrestaurant-toolkit' ),
		),
		array(
			'id'          => 'opt_body_font',
			'type'        => 'typography', 
			'title'       => esc_html__('Body Style', 'maxrestaurant-toolkit'),
			'google'      => true, 
			'font-backup' => false,
			'subsets'      => false,
			'text-align'      => false,
			'line-height'      => false,
			'output'      => array('body'),
			'units'       =>'px',
			'subtitle'    => esc_html__('Body Style', 'maxrestaurant-toolkit'),
		),
		array(
			'id' => 'notice_critical11',
			'type' => 'info',
			'notice' => true,
			'style' => 'critical',
			'icon' => 'fa fa-font',
			'title' => esc_html__('H1 to H6 Styling', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Typography settings H1 to H6 Tags', 'maxrestaurant-toolkit'),
		),
		array(
			'id' => 'h1-font',
			'type' => 'typography',
			'title' => esc_html__('H1', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h1',
		),
		array(
			'id' => 'h2-font',
			'type' => 'typography',
			'title' => esc_html__('H2', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h2',
		),
		array(
			'id' => 'h3-font',
			'type' => 'typography',
			'title' => esc_html__('H3', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h3',
		),
		array(
			'id' => 'h4-font',
			'type' => 'typography',
			'title' => esc_html__('H4', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h4',			
		),
		array(
			'id' => 'h5-font',
			'type' => 'typography',
			'title' => esc_html__('H5', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h5',			
		),
		array(
			'id' => 'h6-font',
			'type' => 'typography',
			'title' => esc_html__('H6', 'maxrestaurant-toolkit'),
			'subtitle' => esc_html__('Specify the Heading font properties.', 'maxrestaurant-toolkit'),
			'google' => true,
			'text-align' =>false,
			'output' => 'h6',
		),
	),
));

/* Documentation */
Redux::setSection( $opt_name, array(
	'title'  => esc_html__( 'Documentation', "maxrestaurant-toolkit" ),
	'icon' => 'fa fa-book',
	'subsection' => false,
	'fields'     => array(
		/* Fields */
		array(
			'id'=>'info_link',
			'type' => 'info',
			'notice' => true,
			'icon' => "el el-file",
			'style' => 'critical',
			'title' => wp_kses( __( '<a href="http://lolthemes.com/demo/_themes/wpm/maxrestaurant_tm/doc/" target="_blank">Read Documentation</a>', "maxrestaurant-toolkit" ), array( 'a' => array( 'target' => array(), 'href' => array() ) ) ),
		),
		/* Fields /- */
	),
) );

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if ( ! function_exists( 'maxrestaurant_remove_demo' ) ) {
	function maxrestaurant_remove_demo() {
		// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
		if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
			remove_filter( 'plugin_row_meta', array(
				ReduxFrameworkPlugin::instance(),
				'plugin_metalinks'
			), null, 2 );

			// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
			remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
		}
	}
}
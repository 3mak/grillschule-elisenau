<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'maxrestaurant_cf_';

/* Post : maxres_gallery */
$cmb_gallery = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_gallery',
	'title'         => esc_html__( 'Gallery Options', "maxrestaurant-toolkit" ),
	'object_types'  => array( 'maxres_gallery' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );
$cmb_gallery->add_field( array(
    'name' => 'Embed Video URL',
    'desc' => 'Enter Embeds URL. Example For: vimeo.com/27209688',
    'id'   => $prefix .'gallery_video_embed',
    'type' => 'oembed',
) );
?>
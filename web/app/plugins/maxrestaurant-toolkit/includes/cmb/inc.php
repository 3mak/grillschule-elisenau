<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

if( ! function_exists("maxrestaurant_get_sidebars") ) {

	function maxrestaurant_get_sidebars() {

		global $wp_registered_sidebars;

		$sidebar_options = array();

		$dwidgetarea = array( "" => "Select an Option" );

		foreach ( $wp_registered_sidebars as $sidebar ) {
			$sidebar_options[$sidebar['id']] = $sidebar['name'];
		}
		return array_merge( $dwidgetarea, $sidebar_options );
	}
}
add_action( 'cmb2_init', 'register_maxrestaurant_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function register_maxrestaurant_metabox() {
	
	$footersidebar = array(
		"none" => "Select Widget Area",
		"sidebar-5" => "Footer Sidebar 1",
		"sidebar-6" => "Footer Sidebar 2",
		"sidebar-7" => "Footer Sidebar 3",
		"sidebar-8" => "Footer Sidebar 4",
		"sidebar-9" => "Footer Sidebar 5",
		"sidebar-10" => "Footer Sidebar 6",
		"sidebar-11" => "Footer Sidebar 7",
		"sidebar-12" => "Footer Sidebar 8"
	);

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'maxrestaurant_cf_';

	/* ## Page/Post Options ---------------------- */

	/* - Page Description */
	$cmb_page = new_cmb2_box( array(
		'id'            => $prefix . 'metabox_page',
		'title'         => esc_html__( 'Page Options', "maxrestaurant-toolkit" ),
		'object_types'  => array( 'page', 'post', 'product','maxres_gallery','maxres_events' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$cmb_page->add_field( array(
		'name'             => 'Content Area Padding',
		'desc'             => 'If your content section need to have just after header area without space, please select an option Off',
		'id'               => $prefix . 'content_padding',
		'type'             => 'select',
		'default'          => 'on',
		'options'          => array(
			'on' => esc_html__( 'On', "maxrestaurant-toolkit" ),
			'off'   => esc_html__( 'Off', "maxrestaurant-toolkit" ),
		),
	) );

	$cmb_page->add_field( array(
		'name'             => 'Page Layout',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'page_owlayout',
		'type'             => 'radio',
		'default'          => 'none',
		'options'          => array(
			'none' =>  '<img title="Default" src="'. MAXRESTAURANT_LIB .'/images/none.png" />',
			'fixed' =>  '<img title="Fixed" src="'. MAXRESTAURANT_LIB .'/images/boxed.png" />',
			'fluid' =>  '<img title="Fluid" src="'. MAXRESTAURANT_LIB .'/images/full.png" />'
		),
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Sidebar Position',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'sidebar_owlayout',
		'type'             => 'radio',
		'default'          => 'none',
		'options'          => array(
			'none' =>  '<img src="'. MAXRESTAURANT_LIB .'/images/none.png" />',
			'right_sidebar' =>  '<img src="'. MAXRESTAURANT_LIB .'/images/right_side.png" />',
			'left_sidebar' =>  '<img src="'. MAXRESTAURANT_LIB .'/images/left_side.png" />',
			'no_sidebar' =>  '<img src="'. MAXRESTAURANT_LIB .'/images/no_side.png" />',
		),
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Widget Area',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'widget_area',
		'type'             => 'select',
		'options'          => maxrestaurant_get_sidebars(),
	) );
	$cmb_page->add_field( array(
		'name'             => 'Page Header',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'page_title',
		'type'             => 'select',
		'default'          => 'enable',
		'options'          => array(
			'enable' => esc_html__( 'Enable', "maxrestaurant-toolkit" ),
			'disable'   => esc_html__( 'Disable', "maxrestaurant-toolkit" ),
		),
	) );

	$cmb_page->add_field( array(
		'name' => esc_html__( 'Header Image', "maxrestaurant-toolkit" ),
		'desc' => esc_html__( 'Upload an image or enter a URL.', "maxrestaurant-toolkit" ),
		'id'   => $prefix . 'page_header_img',
		'type' => 'file',
	) );

	$cmb_page->add_field( array(
		'name' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
		'id'   => $prefix . 'page_subtitle',
		'type' => 'text',
	) );
	
	$cmb_page->add_field( array(
		'name' => esc_html__( 'Page Specific Logo', "maxrestaurant-toolkit" ),
		'id'   => $prefix . 'custom_logo',
		'type' => 'file',
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Header Layout',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'header_layout',
		'type'             => 'select',
		'default'          => '2',
		'options'          => array(
			'' => esc_html__( 'Select Header Layout', "maxrestaurant-toolkit" ),
			'1' => esc_html__( 'Header Menu Black', "maxrestaurant-toolkit" ),
			'2'   => esc_html__( 'Header Menu White', "maxrestaurant-toolkit" ),
		),
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Footer Layout',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'footer_layout',
		'type'             => 'select',
		'options'          => array(
			'' => esc_html__( 'Select Footer Type', "maxrestaurant-toolkit" ),
			'1' => esc_html__( 'Layout 1', "maxrestaurant-toolkit" ),
			'2'   => esc_html__( 'Layout 2', "maxrestaurant-toolkit" ),
		),
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Footer Widget Area 1',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'footer_widget_area1',
		'type'             => 'select',
		'options'          => $footersidebar,
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Footer Widget Area 2',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'footer_widget_area2',
		'type'             => 'select',
		'options'          => $footersidebar,
	) );
	
	$cmb_page->add_field( array(
		'name'             => 'Footer Widget Area 3',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'footer_widget_area3',
		'type'             => 'select',
		'options'          => $footersidebar,
	) );
	$cmb_page->add_field( array(
		'name'             => 'Footer Widget Area 4',
		'desc'             => 'Select an option',
		'id'               => $prefix . 'footer_widget_area4',
		'type'             => 'select',
		'options'          => $footersidebar,
	) );
	
	$prefix_cmb = "cmb_";

	/* ## Post Options ---------------------- */
	require_once( $prefix_cmb . "post.php");

	/* ## Gallery Options ---------------------- */
	require_once( $prefix_cmb . "gallery.php");

    /* ## Team Options ---------------------- */
	require_once( $prefix_cmb . "team.php");

    /* ## Events Options ---------------------- */
	require_once( $prefix_cmb . "events.php");
}
?>
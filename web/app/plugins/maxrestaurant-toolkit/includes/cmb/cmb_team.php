<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'maxrestaurant_cf_';

/* Post : maxres_team */
$cmb_team = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_team',
	'title'         => esc_html__( 'Team Options', "maxrestaurant-toolkit" ),
	'object_types'  => array( 'maxres_team' ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );
$cmb_team->add_field( array(
    'name' => 'Job description',
    'id'   => $prefix .'team_job_description',
    'type' => 'text',
) );
?>
<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'maxrestaurant_cf_';

/* Post : maxres_team */
$cmb_team = new_cmb2_box(array(
    'id' => $prefix . 'metabox_event',
    'title' => esc_html__('Additional Event Options', "maxrestaurant-toolkit"),
    'object_types' => array('tribe_events'), // Post type
    'context' => 'normal',
//    'priority' => 'high',
    'show_names' => true, // Show field names on the left
));

$cmb_team->add_field(array(
    'name' => _('Prospective Event'),
    'description' => _('This will override start and end date for an event.'),
    'id' => $prefix . 'prospective_event',
    'type' => 'checkbox',
));

$cmb_team->add_field(array(
	'name' => _('Sold Out Event'),
	'description' => _('This will declare an event as sold out.'),
	'id' => $prefix . 'sold_out',
	'type' => 'checkbox',
));

$cmb_team->add_field(array(
    'name' => _('Gallery'),
    'id' => $prefix . 'gallery',
    'description' => 'Currently not implemented!',
    'type' => 'file_list',
    'preview_size' => array(100, 100), // Default: array( 50, 50 )
    'query_args' => array('type' => 'image') // Only images attachment
));

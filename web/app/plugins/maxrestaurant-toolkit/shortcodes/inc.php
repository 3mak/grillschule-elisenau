<?php
if( !function_exists('maxrestaurant_sc_setup') ) {
	
	function maxrestaurant_sc_setup() {
		add_image_size( 'maxrestaurant_79_79', 79, 79 ,true  ); /* Recent Post Widget */
		add_image_size( 'maxrestaurant_150_150', 150, 150 ,true  ); /* About Me */
		add_image_size( 'maxrestaurant_86_86', 86, 86 ,true  ); /* About Me Sign */
		add_image_size( 'maxrestaurant_514_265', 514, 265 ,true  ); /* About US */
		add_image_size( 'maxrestaurant_124_92', 124, 92 ,true  ); /* Image block */
		add_image_size( 'maxrestaurant_260_424', 260, 424 ,true  ); /* Team */
		add_image_size( 'maxrestaurant_555_350', 555, 350 ,true  ); /* Blog */
		add_image_size( 'maxrestaurant_304_465', 304, 465 ,true  ); /* Menu Card Top Image */
		add_image_size( 'maxrestaurant_368_417', 368, 417 ,true  ); /* Menu Card Bottom Image */
		add_image_size( 'maxrestaurant_84_84', 84, 84 ,true  ); /* Menu Card & Menu Dinner List Image */ 
		add_image_size( 'maxrestaurant_437_284', 437, 284 ,true  ); /* Menu Dinner List Image */
		add_image_size( 'maxrestaurant_353_264', 353, 264 ,true  ); /* Menu BreakFast List Image */
		add_image_size( 'maxrestaurant_263_289', 263, 289 ,true  ); /* Gallery  */
		add_image_size( 'maxrestaurant_360_532', 360, 532 ,true  ); /* Event */
		add_image_size( 'maxrestaurant_268_270', 268, 270 ,true  ); /* Service */
	}
	add_action( 'after_setup_theme', 'maxrestaurant_sc_setup' );
}

function maxrestaurant_currentYear() {
    return date('Y');
}
add_shortcode( 'year', 'maxrestaurant_currentYear' );

if( function_exists('vc_map') ) {

	vc_add_param("vc_row", array(
		"type" => "dropdown",
		"group" => "Page Layout",
		"class" => "",
		"heading" => "Type",
		"param_name" => "type",
		'value' => array(
			esc_html__( 'Default', "maxrestaurant-toolkit" ) => 'default-layout',
			esc_html__( 'Fixed', "maxrestaurant-toolkit" ) => 'container',
		),
	));
	
	vc_add_param("vc_row", array(
		"type" => "dropdown",
		"group" => "Page Layout",
		"class" => "",
		"heading" => "Section Padding",
		"param_name" => "spadding",
		'value' => array(
			esc_html__( 'No', "maxrestaurant-toolkit" ) => 'no',
			esc_html__( 'Yes', "maxrestaurant-toolkit" ) => 'yes',
		),
	));

	/* Include all individual shortcodes. */
	$prefix_sc = "sc_";

	require_once( $prefix_sc . "contactform.php");
	require_once( $prefix_sc . "contactdetails.php");
	require_once( $prefix_sc . "googlemap.php");
	require_once( $prefix_sc . "about.php");
	require_once( $prefix_sc . "imageblock.php");
	require_once( $prefix_sc . "service.php");
	require_once( $prefix_sc . "counter.php");
	require_once( $prefix_sc . "menuldetail.php");
	require_once( $prefix_sc . "reservationform.php");
	require_once( $prefix_sc . "event.php");
	require_once( $prefix_sc . "testimonial.php");
	require_once( $prefix_sc . "blog.php");
	require_once( $prefix_sc . "newslatter.php");
	require_once( $prefix_sc . "team.php");
	require_once( $prefix_sc . "gallery.php");
	require_once( $prefix_sc . "breakfast.php");
	require_once( $prefix_sc . "menudinner.php");
	require_once( $prefix_sc . "warmdishes.php");
}
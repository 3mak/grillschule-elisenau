<?php
function maxrestaurant_counter( $atts ) {
	
	extract( shortcode_atts( array( 'sc_bg' => '', 'sc_title' => '', 'sc_subtitle' => '' ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	?>
	<!-- Counter Section -->
	<div class="container-fluid no-left-padding no-right-padding counter-section"<?php echo html_entity_decode( $style ); ?>>
		<!-- Container -->
		<div class="container">
			<?php 
			if( $sc_title !="" || $sc_subtitle != "" ) {
				?>
				<!-- Section Header -->
				<div class="section-header text-center">
					<?php if( $sc_title != "" ) { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
					<?php if($sc_subtitle != "" ){ ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
				</div><!-- Section Header /- -->
				<?php
			}
			if( maxrestaurant_options("opt_counter") != "" ) {
				?>
				<!-- Row -->
				<div class="row">
					<?php
						$count_cnt = 1;
						foreach( maxrestaurant_options("opt_counter") as $single_item ) {
							?>
							<div class="col-md-3 col-sm-3 col-xs-6">
								<div class="counter-box">
									<?php if($single_item["attachment_id"] !="" ) { ?><i><?php echo wp_get_attachment_image( $single_item["attachment_id"], 'full' ); ?></i><?php } ?>
									<span class="count" id="statistics_count-<?php echo esc_attr($count_cnt); ?>" data-statistics_percent="<?php echo esc_attr( $single_item["textOne"] ); ?>"> <?php esc_html_e('&nbsp;',"maxrestaurant-toolkit"); ?></span>
									<?php if( $single_item["title"] != "" ) { ?><h6><?php echo esc_attr( $single_item["title"] ); ?></h6><?php } ?>
								</div>
							</div>
							<?php
							$count_cnt++;
						}
					?>
				</div><!-- Row /- -->
				<?php
			}
			?>
		</div><!-- Container /- -->
	</div><!-- Counter Section /- -->
	<div class="clearfix"></div>
	
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_counter', 'maxrestaurant_counter');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_counter',
		'name' => esc_html__( 'Counter/Skill', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
		),
	) );
}
?>
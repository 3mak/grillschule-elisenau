<?php
function maxrestaurant_imgblock( $atts ) {
	
	extract( shortcode_atts( array( 'sc_bg' => '','sc_title' => '','sc_img' => '','sc_url' => '' ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	?>
	<!-- Surprise Section -->
	<div class="container-fluid no-left-padding no-right-padding surprise-section"<?php echo html_entity_decode( $style ); ?>>
		<!-- Container -->
		<div class="container">
			<div class="surprise-content text-center">
				<?php 
					if( $sc_title != "" ) { 
						?><h3><?php echo esc_attr($sc_title); ?></h3>
						<?php 
					} 
					if($sc_img != "" && $sc_url != "" ){
						?>
						<a href="<?php echo esc_url($sc_url); ?>">
							<?php echo wp_get_attachment_image($sc_img,'maxrestaurant_124_92'); ?>
						</a>
						<?php
					}
					else if($sc_img != ""){
						echo wp_get_attachment_image($sc_img,'maxrestaurant_124_92'); 
					}
				?>
			</div>
		</div><!-- Container /- -->
	</div><!-- Surprise Section /- -->
	<div class="clearfix"></div>
	<?php
	return ob_get_clean();
}

add_shortcode('maxrestaurant_imgblock', 'maxrestaurant_imgblock');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_imgblock',
		'name' => esc_html__( 'Image Block', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Icon Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_img',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Image URL', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_url',
			),
		),
	) );
}
?>
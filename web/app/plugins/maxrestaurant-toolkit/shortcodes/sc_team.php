<?php
function maxrestaurant_team( $atts ) {
    $ow_post_type = 'maxres_team';
    $posts_display = 6;

	extract( shortcode_atts( array( 'sc_title' => '', 'sc_subtitle' => '' ), $atts ) );
    $team = get_posts(array(
        'post_type' => $ow_post_type,
        'posts_per_page' => $posts_display,
    ));

	ob_start();
	
	?>
	<!-- Team Section -->
	<div class="container-fluid no-left-padding no-right-padding team-section">
		<!-- Container -->
		<div class="container">
			<?php 
				if($sc_title != "" || $sc_subtitle != "" ) {
					?>
					<!-- Section Header -->
					<div class="section-header text-center">
						<?php if($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
						<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
					</div><!-- Section Header /- -->
					<?php
				} ?>
					<!-- Row -->
					<div class="row">
						<?php foreach ( $team as $member) : setup_postdata($member);?>
							<div class="col-md-3 col-sm-4 col-xs-4">
								<div class="team-box text-center">
									<?php echo get_the_post_thumbnail( $member->ID, 'maxrestaurant_260_424' ); ?>
									<div class="team-content">
                                        <h4><?= $member->post_title; ?></h4>
										<h6><?= get_post_meta($member->ID, 'maxrestaurant_cf_team_job_description')[0]?></h6>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
					</div><!-- Row -->
		</div><!-- Container /- -->
	</div><!-- Team Section /- -->
	<div class="clearfix"></div>
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_team', 'maxrestaurant_team');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_team',
		'name' => esc_html__( 'Our Team', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
		),
	) );
}
?>
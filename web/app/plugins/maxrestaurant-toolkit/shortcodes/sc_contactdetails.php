<?php
function maxrestaurant_contactdetails( $atts ) {
	
	extract( shortcode_atts( array( 'sc_contact_one' => '', 'sc_contact_two' => '', 'sc_mail_one' => '','sc_mail_two' => '','sc_contact_address' => '' ), $atts ) );
	
	ob_start();
	
	?>
	<!-- Contact Details -->
	<div class="container-fluid no-left-padding no-right-padding contact-details">
		<!-- Container -->
		<div class="container">
			<!-- Row -->
			<div class="row row-eq-height">
				<?php
				if( $sc_contact_one != "" || $sc_contact_two != "" ) {
					?>
					<div class="col-md-4 col-sm-4">
						<div class="cnt-detail-box">
							<i><img src="<?php echo esc_url(MAXRESTAURANT_LIB); ?>/images/cnt-phone.png" alt="Phone"/></i>
							<h4><?php esc_html_e('CALL US',"maxrestaurant-toolkit"); ?></h4>						
							<?php if( $sc_contact_one != "" ) { ?><p><a href="tel:<?php echo esc_attr(str_replace(" ", "", $sc_contact_one) ); ?>" title="<?php echo esc_attr(str_replace(" ", "", $sc_contact_one) ); ?>"><?php echo esc_attr($sc_contact_one); ?></a></p><?php } ?>
							<?php if( $sc_contact_two != "" ) { ?><p><a href="tel:<?php echo esc_attr(str_replace(" ", "", $sc_contact_two) ); ?>" title="<?php echo esc_attr(str_replace(" ", "", $sc_contact_two) ); ?>"><?php echo esc_attr($sc_contact_two); ?></a></p><?php } ?>
						</div>
					</div>
					<?php
				}
				if(	$sc_mail_one != "" || $sc_mail_two != "" ) {
					?>
					<div class="col-md-4 col-sm-4">
						<div class="cnt-detail-box">
							<i><img src="<?php echo esc_url(MAXRESTAURANT_LIB); ?>/images/cnt-email.png" alt="EMAIL" /></i>
							<h4><?php esc_html_e('EMAIL','maxrestaurant-toolkit'); ?></h4>
							<?php if($sc_mail_one) { ?><p><a href="mailto:<?php echo esc_attr($sc_mail_one); ?>" title="<?php echo esc_attr($sc_mail_one); ?>"><?php echo esc_attr($sc_mail_one); ?></a></p><?php } ?>
							<?php if($sc_mail_two) { ?><p><a href="mailto:<?php echo esc_attr($sc_mail_two); ?>" title="<?php echo esc_attr($sc_mail_two); ?>"><?php echo esc_attr($sc_mail_two); ?></a></p><?php } ?>
						</div>
					</div>
					<?php
				}
				if( $sc_contact_address != "" ) {
					?>
					<div class="col-md-4 col-sm-4">
						<div class="cnt-detail-box">
							<i><img src="<?php echo esc_url(MAXRESTAURANT_LIB); ?>/images/cnt-marker.png" alt="Marker" /></i>
							<h4><?php esc_html_e('Address','maxrestaurant-toolkit'); ?></h4>
							<p><?php echo esc_attr($sc_contact_address); ?></p>
						</div>
					</div>
					<?php
				} ?>
			</div><!-- Row /- -->
		</div><!-- Container /- -->
	</div><!-- Contact Details /- -->
	<?php
	return ob_get_clean();
}

add_shortcode('maxrestaurant_contactdetails', 'maxrestaurant_contactdetails');

if( function_exists('vc_map') ) {
	
	vc_map( array(
		'base' => 'maxrestaurant_contactdetails',
		'name' => esc_html__( 'Contact Details', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Contact Number 1', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_contact_one',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Contact Number 2', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_contact_two',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Contact Mail 1', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_mail_one',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Contact Mail 2', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_mail_two',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Contact Address', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_contact_address',
			),
		),
	) );
}
?>
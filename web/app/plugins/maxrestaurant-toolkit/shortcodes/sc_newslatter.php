<?php
function maxrestaurant_newslatter( $atts, $content = null  ) {
	
	extract( shortcode_atts( array( 'sc_title' => '', 'sc_subtitle' => '', 'sc_bg' => '' ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	?>
	<!-- Newsletter Section -->
	<div class="container-fluid no-left-padding no-right-padding no-bottom-margin newsletter-section"<?php echo html_entity_decode( $style ); ?>>
		<!-- Container -->
		<div class="container">
			<?php if($sc_title != "" ) { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
			<?php if($sc_subtitle) { ?><p><?php echo esc_attr($sc_subtitle); ?></p><?php } ?>
			<?php echo do_shortcode($content);?>
		</div><!-- Container /- -->
	</div><!-- Newsletter Section /- -->
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_newslatter', 'maxrestaurant_newslatter');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_newslatter',
		'name' => esc_html__( 'Newslatter', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
			array(
				"type" => "textarea_html",
				"class" => "",
				"heading" => esc_html("Newsletter Shortcode", "maxrestaurant-toolkit"),
				"description" => esc_html('e.g : [mc4wp_form id="191"]', "maxrestaurant-toolkit"),
				"param_name" => "content",
				"holder" => "div",
			),
		),
	) );
}
?>
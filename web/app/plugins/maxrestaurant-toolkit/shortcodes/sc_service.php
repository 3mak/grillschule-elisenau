<?php
function maxrestaurant_service( $atts ) {
	
	extract( shortcode_atts( array( 'sc_title' => '', 'sc_subtitle' => '' ), $atts ) );
	
	ob_start();
	
	?>
	<!-- Services Section -->
	<div class="container-fluid no-left-padding no-right-padding services-section">
		<!-- Container -->
		<div class="container">
			<?php
				if( $sc_title != "" || $sc_subtitle != "" ) {
					?>
					<!-- Section Header -->
					<div class="section-header text-center">
						<?php if($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
						<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
					</div><!-- Section Header /- -->
					<?php
				}
				if( maxrestaurant_options("opt_service") != "" ) {
					?>
					<!-- Row -->
					<div class="row">
						<?php 
						foreach( maxrestaurant_options("opt_service") as $single_item ) {
							if( $single_item["attachment_id"] != "" || $single_item["textOne"] != "" || $single_item["title"] != "" ) {
								?>
								<div class="col-md-3 col-sm-4 col-xs-6">
									<div class="srv-box">
										<div class="img-box">
											<?php 
												echo wp_get_attachment_image( $single_item["attachment_id"], 'maxrestaurant_268_270' );
												
												if( $single_item["textOne"] != "" ) {
													?>
													<div class="srv-content">
														<span><?php esc_html_e('ORDER NOW CALL',"maxrestaurant-toolkit"); ?><a href="tel:<?php echo esc_attr(str_replace(" ", "", $single_item["textOne"] ) ); ?>"><?php echo esc_attr( $single_item["textOne"] ); ?></a></span>
													</div>
													<?php
												}
											?>
										</div>
										<?php if( $single_item["title"] != "" ) { ?><h4><?php echo esc_attr( $single_item["title"] ); ?></h4><?php } ?>
									</div>
								</div>
								<?php
							}
						}
						?>
					</div><!-- Row /- -->
					<?php
				}
			?>
		</div><!-- Container /- -->
	</div><!-- Services Section /- -->
	<div class="clearfix"></div>
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_service', 'maxrestaurant_service');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_service',
		'name' => esc_html__( 'Services', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
		),
	) );
}
?>
<?php
function maxrestaurant_warmdishes( $atts ) {
	
	extract( shortcode_atts( array( 'sc_title' => '','sc_subtitle' => '' ), $atts ) );
	
	ob_start();
	
	?>
	<!-- Menu Section -->
	<div class="container-fluid no-left-padding no-right-padding menu-section menu-cards">
		<!-- Container -->
		<div class="container">
			<?php
				if($sc_title != "" || $sc_subtitle != "") {
					?>
					<!-- Section Header -->
					<div class="section-header text-center">
						<?php if($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
						<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
					</div><!-- Section Header /- -->
					<?php
				}
				if( maxrestaurant_options("opt_menuwarmdish") != "" ) {
					?>
					<!-- Row -->
					<div class="row">
						<?php
						foreach( maxrestaurant_options("opt_menuwarmdish") as $single_item ) {
							?>
							<!-- Menu Item -->
							<div class="col-md-6">
								<div class="menu-item">
									<?php
									if($single_item["attachment_id"] != "") {
										?>
										<div class="item-thumb">
											<i><?php echo wp_get_attachment_image( $single_item["attachment_id"], 'maxrestaurant_84_84' ); ?></i>
										</div>
										<?php
									}
									if($single_item["title"] != "" || $single_item["textOne"] != "") {
										?><h3><?php echo esc_attr( $single_item["title"] ); ?><span><?php echo esc_attr( $single_item["textOne"] ); ?></span></h3>
										<?php
									}
									if($single_item["textTwo"] != "") {
										?>
										<span><sup><?php esc_html_e('$',"maxrestaurant-toolkit"); ?></sup><?php echo esc_attr($single_item["textTwo"] ); ?></span>
										<?php
									}
									?>
								</div>
							</div><!-- Menu Item /- -->
							<?php
						}
						?>
					</div><!-- Row /- -->
					<?php
				}	
				?>
		</div><!-- Container /- -->
	</div><!-- Menu Section -->
	
	<div class="clearfix"></div>
	<?php
	return ob_get_clean();
}

add_shortcode('maxrestaurant_warmdishes', 'maxrestaurant_warmdishes');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_warmdishes',
		'name' => esc_html__( 'Menu WarmDish', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
		),
	) );
}
?>
<?php
function maxrestaurant_gallery( $atts ) {
	
	extract( shortcode_atts( array( 'layout' => 'one','title' => '', 'subtitle' => '','posts_display' => '', 'sc_bg' => ''  ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	$ow_post_type = 'maxres_gallery';
	$ow_post_tax = 'maxrestaurant_gallery_tax';
	
	$tax_args = array(
		'hide_empty' => false
	);

	if( '' === $posts_display ) :
		$posts_display = 8;		
	endif;
	
	$qry = new WP_Query( array(
		'post_type' => $ow_post_type,
		'posts_per_page' => $posts_display,
	) );
	
	$unexpected_str = array(" ","&","amp;",",",".","/");
	$terms = get_terms( $ow_post_tax, $tax_args );
	
	ob_start();
	
	if($layout == 'one') {
		?>
		<!-- Gallery Section -->
		<div class="container-fluid no-left-padding no-right-padding gallery-section"<?php echo html_entity_decode( $style ); ?>>
			<!-- Container -->
			<div class="container">
				<?php 
				if( $title != "" || $subtitle != "" ) {
					?>
					<!-- Section Header -->
					<div class="section-header text-center">
						<?php if($title != "") { ?><h3><?php echo esc_attr($title); ?></h3><?php } ?>
						<?php if($subtitle != "") { ?><h4><?php echo esc_attr($subtitle); ?></h4><?php } ?>
					</div><!-- Section Header /- -->
					<?php
				} ?>
				<div class="gallery-category">
					<ul id="filters">
						<li><a data-filter="*" class="active" href="#" title="All"><?php esc_html_e('All',"maxrestaurant-toolkit"); ?></a></li>
						<?php
							if ( count( $terms > 0 ) && is_array( $terms ) ) {
								foreach ( $terms as $term ) {
									$termname = str_replace( $unexpected_str, '-', strtolower($term->name) );
									?>
									<li><a data-filter=".<?php echo esc_attr( $termname ); ?>" href="#" title="<?php echo esc_attr ( $term->name ); ?>"><?php echo esc_attr ( $term->name ); ?></a></li>
									<?php
								}
							}
						?>
					</ul>
				</div>
				<!-- Row -->
				<div class="row">
					<div class="gallery-list gallery-fitrow">
						<?php
						while ( $qry->have_posts() ) : $qry->the_post();
							$taxonomies = "";
							$terms = get_the_terms( get_the_ID(), $ow_post_tax );
							$termsname = array();
							$terms_dashed = array();
							if ( count( $terms > 0 ) && is_array( $terms ) ) {
								foreach ( $terms as $term ) {
									$termsname[] = strtolower( $term->name );
									$unexpected_str = array(" ","&","amp;",",",".","/");
									$terms_dashed[] = str_replace( $unexpected_str, '-', strtolower( $term->name ) );
								}
								$taxonomies = implode(" ", $terms_dashed );
								$taxonomies_plus = implode(" + ", $termsname );
							}
							?>
							<div class="gallery-box col-md-3 col-sm-4 col-xs-4 <?php echo esc_attr($taxonomies); ?>">
								<?php
									$url = "";
									$url = get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true );
									
									if($url != "" ) {
										?>
										<a class="popup-video" href="<?php echo esc_url( get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true ) ); ?>" title="<?php the_title(); ?>">
											<?php echo wp_oembed_get( esc_url ( get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true ) ) ); ?>
										</a>
										<?php
									}
									else {
										?>
										<a class="zoom-in" href="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) ); ?>">
											<?php the_post_thumbnail("maxrestaurant_263_289"); ?>
										</a>
										<?php
									}
								?>
							</div>
							<?php
							
						endwhile;
						
					/* Reset Post Data */
					wp_reset_postdata();
					
					?>
					</div>
				</div><!-- Row -->
			</div><!-- Container /- -->
		</div><!-- Gallery Section /- -->
		<div class="clearfix"></div>
		<?php
	}
	elseif($layout == 'two') {
		?>
		<!-- Gallery Section -->
		<div class="container-fluid no-padding gallery-section gallery-page">
			<!-- Container -->
			<div class="container">
				<!-- Section Header -->
				<div class="section-header text-center">
					<?php if($title != "") { ?><h3><?php echo esc_attr($title); ?></h3><?php } ?>
					<?php if($subtitle != "" ) { ?><h4><?php echo esc_attr($subtitle); ?></h4><?php } ?>
				</div><!-- Section Header /- -->
				<div class="gallery-category">
					<ul id="filters">
						<li><a data-filter="*" class="active" href="#" title="All"><?php esc_html_e('All',"maxrestaurant-toolkit"); ?></a></li>
						<?php
							if ( $terms > 0 && is_array( $terms ) ) {
								foreach ( $terms as $term ) {
									$termname = str_replace( $unexpected_str, '-', strtolower($term->name) );
									?>
									<li><a data-filter=".<?php echo esc_attr( $termname ); ?>" href="#" title="<?php echo esc_attr ( $term->name ); ?>"><?php echo esc_attr ( $term->name ); ?></a></li>
									<?php
								}
							}
						?>
					</ul>
				</div>
				<!-- Row -->
				<div class="row">
					<div class="gallery-list gallery-fitrow">
						<?php
							while ( $qry->have_posts() ) : $qry->the_post();
								$taxonomies = "";
								$terms = get_the_terms( get_the_ID(), $ow_post_tax );
								$termsname = array();
								$terms_dashed = array();
								if ( $terms > 0 && is_array( $terms ) ) {
									foreach ( $terms as $term ) {
										$termsname[] = strtolower( $term->name );
										$unexpected_str = array(" ","&","amp;",",",".","/");
										$terms_dashed[] = str_replace( $unexpected_str, '-', strtolower( $term->name ) );
									}
									$taxonomies = implode(" ", $terms_dashed );
									$taxonomies_plus = implode(" + ", $termsname );
								}
								?>
								<div class="gallery-box col-md-3 col-sm-4 col-xs-4 <?php echo esc_attr($taxonomies); ?>">
									<?php
										$url = "";
										$url = get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true );
										
										if($url != "" ) {
											?>
											<a class="popup-video" href="<?php echo esc_url( get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true ) ); ?>" title="<?php the_title(); ?>">
												<?php echo wp_oembed_get( esc_url ( get_post_meta( get_the_ID(), 'maxrestaurant_cf_gallery_video_embed', true ) ) );	?>
											</a>
											<?php
										}
										else {
											?>
											<a class="zoom-in" href="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ) ); ?>">
												<?php the_post_thumbnail("maxrestaurant_263_289"); ?>
											</a>
											<?php
										}
									?>
								</div>
								<?php
								
							endwhile;
							/* Reset Post Data */
							wp_reset_postdata();
						?>
					</div>
				</div><!-- Row -->
			</div><!-- Container /- -->
		</div><!-- Gallery Section /- -->
		<?php
	}
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_gallery', 'maxrestaurant_gallery');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_gallery',
		'name' => esc_html__( 'Gallery', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select a Layout', "maxrestaurant-toolkit" ),
				'param_name' => 'layout',
				'description' => esc_html__( 'Default Layout 1 Set', 'maxrestaurant-toolkit' ),
				'value' => array(
					esc_html__( 'Layout 1', "maxrestaurant-toolkit" ) => 'one',
					esc_html__( 'Layout 2', "maxrestaurant-toolkit" ) => 'two',
				),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'title',
				'holder' => 'div',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'subtitle',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__("Post Per Page Display", "maxrestaurant-toolkit"),
				"param_name" => "posts_display",
				"holder" => "div",
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
		),
	) );
}
?>

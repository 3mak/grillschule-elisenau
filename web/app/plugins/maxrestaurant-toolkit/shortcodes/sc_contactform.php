<?php
function maxrestaurant_contactus( $atts ) {
	
	extract( shortcode_atts( array( 'sc_title' => '', 'id' => '', 'title_layout' =>'one' ), $atts ) );
	
	ob_start();
	
	?>
	<!-- Contact Form -->
	<div class="container-fluid no-left-padding no-right-padding contact-section">
		<!-- Container -->
		<div class="container">
			<div class="col-xs-12 no-left-padding no-right-padding contact-form">
				<?php 
					if($title_layout == 'one') {
						if($sc_title != ""){
							?>
							<div class="col-xs-12 ">
								<h3 class="title-left"><?php echo esc_attr($sc_title);?></h3>
							</div>
							<?php
						}
					}
					elseif($title_layout == 'two') {
						if($sc_title != ""){
							?>
							<div class="col-xs-12 ">
								<h3 class="title-center"><?php echo esc_attr($sc_title);?></h3>
							</div>
							<?php
						}
					}
					elseif($title_layout == 'three') {
						if($sc_title != ""){
							?>
							<div class="col-xs-12 ">
								<h3 class="title-right"><?php echo esc_attr($sc_title);?></h3>
							</div>
							<?php
						}
					}
					if( $id != "" ) {
						?>
						<div class="col-xs-12 no-left-padding no-right-padding">
							<?php echo do_shortcode('[contact-form-7 id="'.esc_attr( $id ).'"]'); ?>
						</div>
						<?php
					}
				?>
			</div>
		</div><!-- Container -->
	</div><!-- Contact Form /- -->
	<?php
	return ob_get_clean();
}

add_shortcode('maxrestaurant_contactus', 'maxrestaurant_contactus');

if( function_exists('vc_map') ) {
	
	/**
	 * Add Shortcode To Visual Composer
	*/
	$maxrestaurant_cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

	$maxrestaurant_contactforms = array();
	
	if ( $maxrestaurant_cf7 ) {
		foreach ( $maxrestaurant_cf7 as $cform ) {
			$maxrestaurant_contactforms[ $cform->post_title ] = $cform->ID;
		}
	} else {
		$maxrestaurant_contactforms[ esc_html__( 'No contact forms found', 'maxrestaurant-toolkit' ) ] = 0;
	}
	
	vc_map( array(
		'base' => 'maxrestaurant_contactus',
		'name' => esc_html__( 'Contact Form', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Title Layout', "maxrestaurant-toolkit" ),
				'param_name' => 'title_layout',
				'description' => esc_html__( 'Default Text Left', "maxrestaurant-toolkit" ),
				'value' =>array(
					esc_html__('Text Left',"maxrestaurant-toolkit") => 'one',
					esc_html__('Text Center',"maxrestaurant-toolkit") => 'two',
					esc_html__('Text Right',"maxrestaurant-toolkit") => 'three',
				),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select Contact Form', 'maxrestaurant-toolkit' ),
				'param_name' => 'id',
				'value' => $maxrestaurant_contactforms,
				'save_always' => true,
				'description' => esc_html__( 'Choose previously created contact form from the drop down list.', 'maxrestaurant-toolkit' ),
			),
		),
	) );
}
?>
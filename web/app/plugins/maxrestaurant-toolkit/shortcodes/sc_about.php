<?php
function maxrestaurant_about( $atts, $content ) {

	
	extract( shortcode_atts( array( 'layout' => 'one', 'sc_title' => '','sc_subtitle' => '', 'sc_about_img' => '','sc_sign_img' => '','sc_about_title' => '','sc_fb_url' => '','sc_tw_url' => '','sc_gp_url' => '', 'sc_fullimg_one' => '','sc_fullimg_two' => '','sc_btn_txt' => '','sc_btn_url' => '' ), $atts ) );
	
	ob_start();
	
	if($layout == 'one') {
		?>
		<!-- About Section -->
		<div class="container-fluid no-left-padding no-right-padding about-section">
			<!-- Container -->
			<div class="container">
				<!-- Row -->
				<div class="row">
					<div class="col-md-6 about-detail">
						<?php
							if( $sc_title != "" || $sc_subtitle != "" ){
								?>
								<!-- Section Header -->
								<div class="section-header section-header2">
									<?php if ($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php }?>
									<?php if ($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php }?>
								</div><!-- Section Header /- -->
								<?php
							}
							echo wpautop($content);
						?>
						<div class="chef-detail">
							<?php if($sc_about_img != "") { ?><i><?php echo wp_get_attachment_image($sc_about_img,'maxrestaurant_150_150'); ?></i><?php } ?>
							<?php if($sc_sign_img != "") { ?><span><?php echo wp_get_attachment_image($sc_sign_img,'maxrestaurant_86_86'); ?></span><?php } ?>
							<div class="cnt">
								<?php 
									if ($sc_about_title != "") { 
										?><h4><?php echo esc_attr($sc_about_title); ?></h4>
										<?php 
									}
									if( $sc_fb_url != "" || $sc_tw_url != "" || $sc_gp_url != "" ) {
										?>
										<ul>
											<?php if($sc_fb_url != "") { ?><li><a href="<?php echo esc_url($sc_fb_url); ?>" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li><?php } ?>
											<?php if($sc_tw_url != "") { ?><li><a href="<?php echo esc_url($sc_tw_url); ?>" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li><?php } ?>
											<?php if($sc_gp_url != "") { ?><li><a href="<?php echo esc_url($sc_gp_url); ?>" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a></li><?php } ?>
										</ul>
										<?php
									}
								?>
							</div>
						</div>
					</div>
					<div class="col-md-6 about-img-block no-padding">
						<?php
						if($sc_fullimg_one != "") {
							?>
							<div class="col-md-12 col-sm-6 col-xs-6">
								<?php echo wp_get_attachment_image($sc_fullimg_one,'maxrestaurant_514_265'); ?>
							</div>
							<?php
						}
						if($sc_fullimg_two != "") {
							?>
							<div class="col-md-12 col-sm-6 col-xs-6">
								<?php echo wp_get_attachment_image($sc_fullimg_two,'maxrestaurant_514_265'); ?>
							</div>
							<?php
						} ?>
					</div>
				</div><!-- Row /- -->
			</div><!-- Container /- -->
		</div><!-- About Section /- -->
		<?php
	}
	elseif($layout == 'two') {
		?>
		<!-- About Section -->
		<div class="container-fluid no-left-padding no-right-padding about-section about-section2">
			<!-- Container -->
			<div class="container">
				<!-- Row -->
				<div class="row">
					<div class="col-md-7 about-detail text-center">
						<?php 
							if( $sc_title != "" || $sc_subtitle != "" ) {
								?>
								<!-- Section Header -->
								<div class="section-header section-header2 text-center">
									<?php if($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
									<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
								</div><!-- Section Header /- -->
								<?php
							}
							echo $content;

							if( $sc_btn_txt != "" ) {
								?>
								<a href="<?php echo esc_url($sc_btn_url); ?>" title="<?php echo esc_attr($sc_btn_txt); ?>"><?php echo esc_attr($sc_btn_txt); ?></a>
								<?php
							}
						?>
					</div>
				</div><!-- Row /- -->
			</div><!-- Container /- -->
			<?php if( $sc_fullimg_one !="" ) { ?><div class="about-img"><?php echo wp_get_attachment_image($sc_fullimg_one,'full'); ?></div><?php } ?>
		</div><!-- About Section /- -->
		<div class="clearfix"></div>
		<?php
	}
	return ob_get_clean();
}

add_shortcode('maxrestaurant_about', 'maxrestaurant_about');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_about',
		'name' => esc_html__( 'About', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select a Layout', "maxrestaurant-toolkit" ),
				'param_name' => 'layout',
				'description' => esc_html__( 'Default Layout 1 Set', 'maxrestaurant-toolkit' ),
				'value' => array(
					esc_html__( 'Layout 1', "maxrestaurant-toolkit" ) => 'one',
					esc_html__( 'Layout 2', "maxrestaurant-toolkit" ) => 'two',
				),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),		
			array(
				'type' => 'textarea_html',
				'heading' => esc_html__( 'Description', "maxrestaurant-toolkit" ),
				'param_name' => 'content',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button Text', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_btn_txt',
				"dependency" => Array('element' => "layout", 'value' => array( 'two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Button URL', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_btn_url',
				"dependency" => Array('element' => "layout", 'value' => array( 'two' ) ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'About Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_about_img',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'About Sign Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_sign_img',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Author Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_about_title',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Facebook URL', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_fb_url',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Twitter URL', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_tw_url',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'GooglePlus URL', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_gp_url',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'About Image 1', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_fullimg_one',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'About Image 2', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_fullimg_two',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
		),
	) );
}
?>
<?php
function maxrestaurant_event( $atts ) {

	extract( shortcode_atts( array( 'sc_title' => '', 'sc_subtitle' => '', 'posts_display' => '' ), $atts ) );

	$ow_post_type = 'tribe_events';

	if ( '' === $posts_display ) :
		$posts_display = 3;
	endif;

	$events = tribe_get_events( [
		'posts_per_page' => $posts_display,
		'start_date'     => 'now',
	] );

	function is_in_stock( $ticket1, $ticket2 ) {
		return ( is_object( $ticket1 ) && $ticket1->is_in_stock() )
               || ( is_object( $ticket2 ) && $ticket2->is_in_stock() );
	}


	ob_start();

	?>
    <!-- Latest Events -->
    <div class="container-fluid no-left-padding no-right-padding latest-events-section">
        <!-- Container -->
        <div class="container">
			<?php
			if ( $sc_title != "" || $sc_subtitle != "" ) {
				?>
                <!-- Section Header -->
                <div class="section-header text-center">
					<?php if ( $sc_title != "" ) { ?><h3><?php echo esc_attr( $sc_title ); ?></h3><?php } ?>
					<?php if ( $sc_subtitle != "" ) { ?><h4><?php echo esc_attr( $sc_subtitle ); ?></h4><?php } ?>
                </div><!-- Section Header /- -->
				<?php
			} ?>
            <!-- Row -->
            <div class="row">
				<?php
				foreach ( $events as $event ) : setup_postdata( $event );
					$event_meta = get_post_custom( $event->ID );
					$startDate  = new DateTime( $event_meta['_EventStartDate'][0] );
					$tickets    = Tribe__Tickets__Tickets::get_event_tickets( $event->ID );
					$is_in_stock = array_reduce( $tickets, "is_in_stock" );
					$sold_out = empty(get_post_meta($event->ID, 'maxrestaurant_cf_sold_out', true)) ? false : true;
					?>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="events-item">
							<?= get_the_post_thumbnail( $event, 'maxrestaurant_360_532' ) ?>
                            <?php if(!$is_in_stock || $sold_out): ?>
                                <div class="badge--default is-rotated">Ausverkauft</div>
                            <?php endif; ?>
                            <div class="event-content">
                                <h4>
									<?= $event->post_title ?>
                                </h4>
								<?php if ( $startDate ): ?>
                                    <span class="event-date">
                                        <i class="fa fa-calendar"></i><?php esc_html_e( ' Termin: ', "maxrestaurant-toolkit" ); ?> <?php echo $startDate->format( 'd.m.Y' ); ?>
                                    </span>
								<?php else: ?>
                                    <span class="event-date"><i class="fa fa-clock-o"></i> Demnächst verfügbar</span>
								<?php endif; ?>

								<?php

								if ( get_the_content() != "" ) {
									echo wpautop( wp_kses( wp_html_excerpt( strip_shortcodes( get_the_content() ), 100, ' [...]' ), maxrestaurant_striptags() ) );
								}
								?>

                                <div class="event-price">
                                    <?php echo tribe_get_cost( $event->ID, true ) ?>
                                </div>

                                <a href="<?= get_the_permalink( $event ) ?>"
                                   title="<?php esc_html_e( 'Mehr Informationen', "maxrestaurant-toolkit" ); ?>">
									<?php esc_html_e( 'Mehr Informationen', "maxrestaurant-toolkit" ); ?>
                                </a>
                            </div>
                        </div>
                    </div>
				<?php

				endforeach;

				/* Reset Post Data */
				wp_reset_postdata();
				?>
            </div><!-- Row /- -->
        </div><!-- Container /- -->
    </div><!-- Latest Events /- -->
    <div class="clearfix"></div>
	<?php
	return ob_get_clean();
}

add_shortcode( 'maxrestaurant_event', 'maxrestaurant_event' );

if ( function_exists( 'vc_map' ) ) {

	vc_map( array(
		'base'     => 'maxrestaurant_event',
		'name'     => esc_html__( 'Event', "maxrestaurant-toolkit" ),
		'class'    => '',
		"category" => esc_html__( "Maxrestaurant Theme", "maxrestaurant-toolkit" ),
		'params'   => array(
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder'     => 'div',
			),
			array(
				'type'       => 'textfield',
				'heading'    => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
			array(
				"type"       => "textfield",
				"class"      => "",
				"heading"    => esc_html__( "Post Per Page Display", "maxrestaurant-toolkit" ),
				"param_name" => "posts_display",
				"holder"     => "div",
			),
		),
	) );
}
?>

<?php
function maxrestaurant_testimonial( $atts ) {

	extract( shortcode_atts( array( 'sc_title' => '','sc_subtitle' => '','sc_bg' => '' ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	?>
	<!-- Testimonial Section -->
	<div class="container-fluid no-left-padding no-right-padding testimonial-section"<?php echo html_entity_decode( $style ); ?>>
		<!-- Container -->
		<div class="container">
			<?php 
				if( $sc_title != "" || $sc_subtitle != "" ) {
					?>
					<!-- Section Header -->
					<div class="section-header text-center">
						<?php if( $sc_title != "" ) { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
						<?php if( $sc_subtitle != "" ) { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
					</div><!-- Section Header /- -->
					<?php
				}
				if( maxrestaurant_options("opt_testimonial") != "" ) {
					?>
					<div class="row">
						<div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10 text-center">
							<div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<?php 
									$testi_cnt = 0;
									foreach( maxrestaurant_options("opt_testimonial") as $single_item ) { 
										?>
										<div class="item<?php if($testi_cnt == 0) { echo ' active'; } ?>">
											<?php echo wpautop( wp_kses( $single_item["description"], maxrestaurant_striptags() ) ); ?>
											<i><i class="fa fa-quote-left" aria-hidden="true"></i></i>
											<?php if($single_item["title"] != "") { ?><h5><?php esc_html_e('- ',"maxrestaurant-toolkit"); ?><?php echo esc_attr($single_item["title"] ); ?></h5><?php } ?>
										</div>
										<?php
									$testi_cnt++;
									}
									?>
								</div>
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<?php
									for( $slide_cnt = 0; $slide_cnt < $testi_cnt; $slide_cnt++ ) {
										?>
										<li data-target="#testimonial-carousel" data-slide-to="<?php echo esc_attr( $slide_cnt ); ?>" <?php if( $slide_cnt == 0 ) { echo ' class="active"'; } ?>></li>
										<?php
									}
									?>
								</ol>
							</div>
						</div>
					</div>
					<?php
				}
			?>
		</div><!-- Container /- -->
	</div><!-- Testimonial Section /- -->
	<div class="clearfix"></div>
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_testimonial', 'maxrestaurant_testimonial');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_testimonial',
		'name' => esc_html__( 'Testimonial', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),
		),
	) );
}
?>
<?php
function maxrestaurant_resform( $atts ) {
	
	extract( shortcode_atts( array( 'layout' => 'one', 'id' => '', 'sc_bg' => '', 'sc_image' => '', 'sc_title' => '','sc_subtitle' => '' ), $atts ) );
	
	if($sc_bg != ""){
		$style = " style='background-image: url(".wp_get_attachment_url( $sc_bg ).");'";
	}
	else {
		$style = "";
	}
	
	ob_start();
	
	if($layout == 'one') {
		?>
		<div class="container-fluid no-left-padding no-right-padding book-table-section"<?php echo html_entity_decode( $style ); ?>>
			<!-- Container -->
			<div class="container">
				<?php 
					if( $sc_title != "" || $sc_subtitle != "" ) {
						?>
						<!-- Section Header -->
						<div class="section-header section-header2 text-center">
							<?php if( $sc_title !="" ) { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
							<?php if($sc_subtitle != "" ) { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
						</div><!-- Section Header /- -->
						<?php
					}
					if($id != "") {
						?>
						<div class="row">
							<?php echo do_shortcode('[contact-form-7 id="'.esc_attr( $id ).'"]'); ?>
						</div>
						<?php
					}
				?>
			</div><!-- Container /- -->
		</div>
		<div class="clearfix"></div>
		<?php
	}
	elseif($layout == 'two') {
		?>
		<!-- Reservation Block -->
		<div class="container-fluid no-left-padding no-right-padding reservation-block">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
					if( $sc_title != "" || $sc_subtitle != "" ) {
						?>
						<!-- Section Header -->
						<div class="section-header text-center">
							<?php if($sc_title != "") { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
							<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
						</div><!-- Section Header /- -->
						<?php
					}
				?>
			</div>
			<?php 
			if($sc_image != "") {
					?>
				<div class="col-md-5 col-sm-12 col-xs-12 reservation-img">
					<?php echo wp_get_attachment_image($sc_image,'full'); ?>
				</div>
				<?php
			}
			if($id != "") {
				?>
				<div class="col-md-7 col-sm-12 col-xs-12 reservation-form-block">
					<div class="reservation-form">
						<?php echo do_shortcode('[contact-form-7 id="'.esc_attr( $id ).'"]'); ?>
					</div>
				</div>
				<?php
			}
			?>
		</div><!-- Reservation Block /- -->
		<?php
	}
	return ob_get_clean();
}

add_shortcode('maxrestaurant_resform', 'maxrestaurant_resform');

if( function_exists('vc_map') ) {
	
	/**
	 * Add Shortcode To Visual Composer
	*/
	$maxrestaurant_cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );

	$maxrestaurant_contactforms = array();
	
	if ( $maxrestaurant_cf7 ) {
		foreach ( $maxrestaurant_cf7 as $cform ) {
			$maxrestaurant_contactforms[ $cform->post_title ] = $cform->ID;
		}
	} else {
		$maxrestaurant_contactforms[ esc_html__( 'No contact forms found', 'maxrestaurant-toolkit' ) ] = 0;
	}
	
	vc_map( array(
		'base' => 'maxrestaurant_resform',
		'name' => esc_html__( 'Reservation Form', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select a Layout', "maxrestaurant-toolkit" ),
				'param_name' => 'layout',
				'description' => esc_html__( 'Default Layout 1 Set', 'maxrestaurant-toolkit' ),
				'value' => array(
					esc_html__( 'Layout 1', "maxrestaurant-toolkit" ) => 'one',
					esc_html__( 'Layout 2', "maxrestaurant-toolkit" ) => 'two',
				),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Background Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_bg',
				"dependency" => Array('element' => "layout", 'value' => array( 'one' ) ),
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_image',
				"dependency" => Array('element' => "layout", 'value' => array( 'two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Select Contact Form', 'maxrestaurant-toolkit' ),
				'param_name' => 'id',
				'value' => $maxrestaurant_contactforms,
				'save_always' => true,
				'description' => esc_html__( 'Choose previously created contact form from the drop down list.', 'maxrestaurant-toolkit' ),
				"dependency" => Array('element' => "layout", 'value' => array( 'one','two' ) ),
			),
		),
	) );
}
?>
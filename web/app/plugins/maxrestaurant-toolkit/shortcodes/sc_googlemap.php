<?php
function maxrestaurant_map( $atts ) {
	
	extract( shortcode_atts( array( 'vc_map_lati' => '','vc_map_longi' => '', 'vc_address' => '', 'vc_marker' => '', 'vc_zoomlevel' => '' ), $atts ) );
	
	if( '' === $vc_zoomlevel ) :
		$vc_zoomlevel = 10;		
	endif;
	
	ob_start();
	
	if( $vc_map_lati != "" && $vc_map_longi != "" ) {
		?>
		<!-- Map Section -->
		<div class="map-section container-fluid no-left-padding no-right-padding">
			<div class="map-canvas" id="map-canvas-contact" data-lat="<?php echo esc_html( $vc_map_lati ); ?>" data-lng="<?php echo esc_html( $vc_map_longi ); ?>" data-string="<?php echo esc_html( $vc_address ); ?>" data-marker="<?php if($vc_marker != "" ){ echo esc_url(wp_get_attachment_url($vc_marker,"full")); } else { echo esc_url( MAXRESTAURANT_LIB ).'images/marker.png'; }?>" data-zoom="<?php echo esc_attr($vc_zoomlevel); ?>"></div>
		</div><!--  Map Section /- -->
		<?php
	}
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_map', 'maxrestaurant_map');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_map',
		'name' => esc_html__( 'Google Map', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Map Latitute', "maxrestaurant-toolkit" ),
				'param_name' => 'vc_map_lati',
				"description" => esc_html("e.g : 40.678178", "maxrestaurant-toolkit"),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Map Longitute', "maxrestaurant-toolkit" ),
				'param_name' => 'vc_map_longi',
				"description" => esc_html("e.g : -73.944158", "maxrestaurant-toolkit"),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Marker Address', "maxrestaurant-toolkit" ),
				'param_name' => 'vc_address',
				"description" => esc_html("e.g : 121 King St, Melbourne VIC 3000, Australia.", "maxrestaurant-toolkit"),
				'holder' => 'div',
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_html__("Map ZoomLevel", "maxrestaurant-toolkit"),
				"param_name" => "vc_zoomlevel",
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Marker Image', "maxrestaurant-toolkit" ),
				'param_name' => 'vc_marker',
			),
		),
	) );
}
?>
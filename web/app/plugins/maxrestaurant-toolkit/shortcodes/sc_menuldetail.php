<?php
function maxrestaurant_menulist( $atts ) {
	
	extract( shortcode_atts( array( 'sc_image_one' => '','sc_image_two' => '','sc_title' => '','sc_subtitle' => '', ), $atts ) );
	
	ob_start();
	
	?>
	<!-- Menu Card Section -->
	<div class="container-fluid no-left-padding no-right-padding menu-section">
		<?php 
		if($sc_image_one != "") { 
			?>
			<span class="top-img">
				<i><?php echo wp_get_attachment_image($sc_image_one,'maxrestaurant_304_465'); ?></i>
			</span>
			<?php
		}
		if($sc_image_two != "") { 
			?>
			<span class="bottom-img">
				<i><?php echo wp_get_attachment_image($sc_image_two,'maxrestaurant_368_417'); ?></i>
			</span>
			<?php
		}
		?>
		<!-- Container -->
		<div class="container">
			<?php 
			if( $sc_title != "" || $sc_subtitle != "" ) {	
				?>
				<!-- Section Header -->
				<div class="section-header text-center">
					<?php if($sc_title != "" ) { ?><h3><?php echo esc_attr($sc_title); ?></h3><?php } ?>
					<?php if($sc_subtitle != "") { ?><h4><?php echo esc_attr($sc_subtitle); ?></h4><?php } ?>
				</div><!-- Section Header /- -->
				<?php
			}
			if( maxrestaurant_options("opt_menucard") != "" ) {
				?>
				<!-- Row -->
				<div class="row">
					<?php
					foreach( maxrestaurant_options("opt_menucard") as $single_item ) { 
						?>
						<!-- Menu Item -->
						<div class="col-md-6">
							<div class="menu-item">
								<?php
								if($single_item["attachment_id"] != ""){
									?>
									<div class="item-thumb">
										<i><?php echo wp_get_attachment_image( $single_item["attachment_id"], 'maxrestaurant_84_84' ); ?></i>
									</div>
									<?php
								}
								if($single_item["title"] != "" || $single_item["textOne"] != "" ){ 
									?><h3><?php echo esc_attr($single_item["title"] ); ?><span><?php echo esc_attr($single_item["textOne"] ); ?></span></h3>
									<?php
								}
								if($single_item["textTwo"] != "") {
									?><span><sup><?php esc_html_e('$',"maxrestaurant-toolkit"); ?></sup><?php echo esc_attr($single_item["textTwo"] ); ?></span>
									<?php
								}
								?>
							</div>
						</div><!-- Menu Item /- -->
						<?php
					}
					?>
				</div><!-- Row /- -->
				<?php
			}
			?>
		</div><!-- Container /- -->
	</div><!-- Menu Card Section /- -->
	<div class="clearfix"></div>
	
	<?php
	
	return ob_get_clean();
}

add_shortcode('maxrestaurant_menulist', 'maxrestaurant_menulist');

if( function_exists('vc_map') ) {

	vc_map( array(
		'base' => 'maxrestaurant_menulist',
		'name' => esc_html__( 'Menu Card', "maxrestaurant-toolkit" ),
		'class' => '',
		"category" => esc_html__("Maxrestaurant Theme", "maxrestaurant-toolkit"),
		'params' => array(
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Menu Item Top Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_image_one',
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__( 'Menu Item Bottom Image', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_image_two',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_title',
				'holder' => 'div',
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', "maxrestaurant-toolkit" ),
				'param_name' => 'sc_subtitle',
			),		
		),
	) );
}
?>
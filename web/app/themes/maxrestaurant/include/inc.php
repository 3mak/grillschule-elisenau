<?php
/* Define Constants */
define( 'MAXRESTAURANT_IMGURI', get_template_directory_uri() . '/assets/images' );

/**
 * Register three widget areas.
 *
 * @since Maxrestaurant 1.0
 */
if ( ! function_exists( 'maxrestaurant_widgets_init' ) ) {
	function maxrestaurant_widgets_init() {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Blog Right Sidebar (Default for Blog)', 'maxrestaurant' ),
				'id'            => 'sidebar-1',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Blog Left Sidebar', 'maxrestaurant' ),
				'id'            => 'sidebar-2',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar(
			array(
				'name'          => esc_html__( 'Shop Right Sidebar (Default for Shop)', 'maxrestaurant' ),
				'id'            => 'sidebar-3',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Shop Left Sidebar', 'maxrestaurant' ),
				'id'            => 'sidebar-4',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 1', 'maxrestaurant' ),
				'id'            => 'sidebar-5',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 2', 'maxrestaurant' ),
				'id'            => 'sidebar-6',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 3', 'maxrestaurant' ),
				'id'            => 'sidebar-7',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 4', 'maxrestaurant' ),
				'id'            => 'sidebar-8',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 5', 'maxrestaurant' ),
				'id'            => 'sidebar-9',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 6', 'maxrestaurant' ),
				'id'            => 'sidebar-10',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 7', 'maxrestaurant' ),
				'id'            => 'sidebar-11',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
		register_sidebar(
			array(
				'name'          => esc_html__( 'Footer Sidebar 8', 'maxrestaurant' ),
				'id'            => 'sidebar-12',
				'description'   => esc_html__( 'Appears in the Content section of the site.', 'maxrestaurant' ),
				'before_widget' => '<aside id="%1$s" class="widget %2$s">',
				'after_widget'  => '</aside>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);
	}
	add_action( 'widgets_init', 'maxrestaurant_widgets_init' );
}

require_once trailingslashit( get_template_directory() ) . 'include/nav_walker.php';
require_once trailingslashit( get_template_directory() ) . 'include/page_walker.php';

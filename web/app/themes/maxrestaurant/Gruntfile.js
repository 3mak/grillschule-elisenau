const sass = require('node-sass');
const grunt = require('grunt');

require('load-grunt-tasks')(grunt);

grunt.initConfig({
    sass: {
        options: {
            implementation: sass,
            sourceMap: true
        },
        dist: {
            files: {

                'assets/css/woocommerce.css': 'assets/scss/woocommerce.scss',
                'assets/css/wordpress.css': 'assets/scss/wordpress.scss',
                'assets/css/elements.css': 'assets/scss/elements.scss',
                'assets/css/plugins.css': 'assets/scss/plugins.scss',
                'assets/css/lib.css': 'assets/scss/lib.scss'
            }
        }
    },
    watch: {
        sass: {
            files: ['./assets/scss/**/*.scss'],
            tasks: ['sass'],
            options: {
                spawn: false,
            },
        },
    },
    browserSync: {
        bsFiles: {
            src: ['assets/css/*.css', '**/*.php'],
        },
        options: {
            watchTask: true,
            proxy: "localhost:8080"
        }
    }
});


grunt.registerTask('default', ['sass', 'browserSync', 'watch']);

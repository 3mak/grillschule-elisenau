<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Maxrestaurant
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/admin/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'maxrestaurant_register_required_plugins' );

function maxrestaurant_register_required_plugins() {

	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		array(
			'name' => esc_html__('Maxrestaurant Toolkit', "maxrestaurant"), // The plugin name.
			'slug' => 'maxrestaurant-toolkit', // The plugin slug (typically the folder name).
			'source' => get_template_directory() . '/admin/plugins/maxrestaurant-toolkit.zip', // The plugin source.
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
			'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		array(
			'name' => esc_html__('WPBakery Visual Composer', 'maxrestaurant'), // The plugin name.
			'slug' => 'js_composer', // The plugin slug (typically the folder name).
			'source' => get_template_directory() . '/admin/plugins/js_composer.zip',
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
			'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		array(
			'name' => esc_html__('Revolution Slider', "maxrestaurant"), // The plugin name.
			'slug' => 'revslider', // The plugin slug (typically the folder name).
			'source' => get_template_directory() . '/admin/plugins/revslider.zip',
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
			'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		array(
			'name'               => esc_html__('CMB2', 'maxrestaurant'), // The plugin name.
			'slug'               => 'cmb2', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		array(
			'name'               => esc_html__('Redux Framework', 'maxrestaurant'), // The plugin name.
			'slug'               => 'redux-framework', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		array(
			'name'               => esc_html__('Contact Form 7', 'maxrestaurant'), // The plugin name.
			'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		
		array(
			'name'               => esc_html__('WooCommerce', 'maxrestaurant'), // The plugin name.
			'slug'               => 'woocommerce', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		array(
			'name'               => esc_html__('YITH WooCommerce Wishlist', 'maxrestaurant'), // The plugin name.
			'slug'               => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		array(
			'name'               => esc_html__('Breadcrumb NavXT', 'maxrestaurant'), // The plugin name.
			'slug'               => 'breadcrumb-navxt', // The plugin slug (typically the folder name).
			'required'           => true,
		),
		array(
			'name'               => esc_html__('MailChimp for WordPress', 'maxrestaurant'), // The plugin name.
			'slug'               => 'mailchimp-for-wp', // The plugin slug (typically the folder name).
			'required'           => true,
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'maxrestaurant',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}
<?php
/**
 * The Header for our theme
 *
 * @package WordPress
 * @subpackage Maxrestaurant
 * @since Maxrestaurant 1.0
 */

$rtl_onoff = '';
if ( maxrestaurant_options( 'opt_rtl_switch' ) != '' ) {
	$rtl_onoff = maxrestaurant_options( 'opt_rtl_switch' );
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js"
									<?php
									if ( $rtl_onoff != '' && $rtl_onoff == 1 ) {
									?>
									 dir="rtl"<?php } ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
	$page_title = '';
	$page_title = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_page_title', true );

if ( maxrestaurant_options( 'opt_siteloader' ) != '0' ) {
	get_template_part( 'template-parts/siteloader' );
}

	$addclass = 'header_s header_s2';
if ( get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_header_layout', true ) == 1 ) {
	$addclass = 'header_s header_s1';
} else {
	$addclass = 'header_s header_s2';
}

	$tophdrclass = '';
if ( maxrestaurant_options( 'opt_resno' ) != '' || maxrestaurant_options( 'opt_booknow' ) != '' ) {
	$tophdrclass = ' top-right';
} else {
	$tophdrclass = ' top-right searchform-full';
}
	?>
	<!-- Header Section -->
	<header class="<?php echo esc_attr( $addclass ); ?>">
		<?php
		if ( maxrestaurant_options( 'opt_top_header' ) != '0' ) {
			?>
			<!-- SidePanel -->
			<div id="slidepanel">
			<!-- Top Header -->
			<div class="container-fluid no-right-padding no-left-padding top-header1">
				<!-- Container -->
				<div class="container">
					<!-- Row -->
					<div class="row">
						<?php
						if ( maxrestaurant_options( 'opt_resno' ) != '' || maxrestaurant_options( 'opt_booknow' ) != '' ) {
							?>
							<div class="col-md-6 col-sm-6 col-xs-12 top-left">
							<?php
							if ( maxrestaurant_options( 'opt_resno' ) != '' ) {
?>
<span><?php esc_html_e( 'Reservierung: ', 'maxrestaurant' ); ?><a href="tel:<?php echo esc_attr( str_replace( ' ', '', maxrestaurant_options( 'opt_resno' ) ) ); ?>"><?php echo esc_attr( maxrestaurant_options( 'opt_resno' ) ); ?></a></span><?php } ?>
							<?php
							if ( maxrestaurant_options( 'opt_booknow' ) != '' ) {
?>
<button type="button" class="btn menupopup_btn" data-toggle="modal" data-target="#myModal"><i class="fa fa-calendar"></i><?php esc_html_e( 'BOOK NOW', 'maxrestaurant' ); ?></button><?php } ?>
								</div>
								<?php
						}
							?>
							<div class="col-md-6 col-sm-6 col-xs-12<?php echo esc_attr( $tophdrclass ); ?>">
							<?php
							if ( maxrestaurant_options( 'opt_social_icons' ) != '' ) {
								?>
								<ul class="top-social">
								<?php get_template_part( 'template-parts/social_icons' ); ?>
									</ul>
									<?php
							}
								?>
								<div class="search-block">
								<?php get_search_form(); ?>
								</div>
								</div>
							</div><!-- Row /- -->
						</div><!-- Container /- -->
					</div><!-- Top Header /- -->
				</div><!-- SidePanel /- -->
				<?php
		}
		?>
		<!-- Ownavigation -->
		<nav class="navbar ownavigation">
			<!-- Container -->
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only"><?php esc_html_e( 'Toggle navigation', 'maxrestaurant' ); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php get_template_part( 'template-parts/sitelogo' ); ?>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<?php get_template_part( 'template-parts/sitemenu' ); ?>
				</div>
				<?php
				if ( maxrestaurant_options( 'opt_top_header' ) != '0' ) {
					?>
					<div id="loginpanel" class="desktop-hide">
					<div class="right" id="toggle">
						<a id="slideit" href="#slidepanel"><i class="fo-icons fa fa-inbox"></i></a>
						<a id="closeit" href="#slidepanel"><i class="fo-icons fa fa-close"></i></a>
					</div>
					</div>
					<?php
				}
				?>
			</div><!-- Container /- -->
		</nav><!-- Ownavigation /- -->
	</header><!-- Header Section /- -->
	<?php
	if ( function_exists( 'maxrestaurant_booknow' ) && maxrestaurant_options( 'opt_booknow' ) != '' ) {
		echo maxrestaurant_booknow();
	}
	if ( $page_title != 'disable' || is_search() ) {
		get_template_part( 'template-parts/pageheader' );
	}
	?>

<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();
$event_meta = get_post_custom(get_the_id());
$tickets    = Tribe__Tickets__Tickets::get_event_tickets( $event_id );

function is_in_stock( $ticket1, $ticket2 ) {
	return ( is_object( $ticket1 ) && $ticket1->is_in_stock() )
	       || ( is_object( $ticket2 ) && $ticket2->is_in_stock() );
}

$is_in_stock = array_reduce( $tickets, "is_in_stock" );
$sold_out = get_post_meta(get_the_id(), 'maxrestaurant_cf_sold_out', true) ? true : false;

?>

<div class="event-single-content woocommerce container">

    <div class="row">

        <div class="eventcontent-block product">
			<?php while ( have_posts() ) : the_post(); ?>

                <div class="col-md-4 col-sm-6 col-xs-6 ">
                    <picture>
                        <source media="(min-width: 38em)" srcset="<?= get_the_post_thumbnail_url($event_id, 'maxrestaurant_360_532')?>">
                        <source srcset="<?= get_the_post_thumbnail_url($event_id, 'medium')?>">
	                    <?php the_post_thumbnail( 'maxrestaurant_360_532' ); ?>
                    </picture>

	                <?php if(!$is_in_stock || $sold_out): ?>
                        <div class="badge--default is-rotated">Ausverkauft</div>
	                <?php endif; ?>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-6">

                    <!--Event Details-->
                    <div class="event-details">
                        <i class="fa fa-calendar-o"></i><?php echo tribe_events_event_schedule_details( $event_id  ); ?>
                    </div>

                    <!-- Notices -->
                    <?php tribe_the_notices() ?>

                    <!--Title-->
                    <?php the_title( '<h1 class="product_title entry-title">', '</h1>' ); ?>
                    <?php if ( tribe_get_cost() ) : ?>
                        <p><span class="price"><?php echo tribe_get_cost( null, true ) ?></span> Preis pro Person</p>
                    <?php endif; ?>

                    <!-- Content-->
                    <div class="event-content">
                        <?php the_content(); ?>
                    </div>

            </div>

            <div class="col-xs-12">
                <!-- .tribe-events-single-event-description -->
				<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

                <!-- Event meta -->
				<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
				<?php  tribe_get_template_part( 'modules/meta' ); ?>
				<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
				<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) )
					comments_template() ?>

            </div>
			<?php endwhile; ?>
        </div>
    </div>
</div>

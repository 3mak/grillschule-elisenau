<?php
/**
 * Theme functions and definitions
 */

/* Include Core */
require get_template_directory() . '/include/inc.php';

/* Include Admin */
require get_template_directory() . '/admin/inc.php';


if (!function_exists('maxrestaurant_get_the_ID')) :

    function maxrestaurant_get_the_ID()
    {

        if (class_exists('WooCommerce') && wc_get_page_id('shop') != '-1' && is_shop()) {
            $post_id = wc_get_page_id('shop');
        } else {
            $post_id = get_the_ID();
        }

        return !empty($post_id) ? $post_id : false;
    }
endif;


/* Redux Options */
if (!function_exists('maxrestaurant_options')) :

    function maxrestaurant_options($option, $arr = null)
    {

        global $maxrestaurant_option;

        if ($arr) {

            if (isset($maxrestaurant_option[$option][$arr])) {
                return $maxrestaurant_option[$option][$arr];
            }
        } else {
            if (isset($maxrestaurant_option[$option])) {
                return $maxrestaurant_option[$option];
            }
        }
    }

endif;


if (!function_exists('maxrestaurant_allowhtmltags')) :

    // Create function which allows more tags within comments
    function maxrestaurant_allowhtmltags()
    {

        global $maxrestaurant_allowedtags;

        $maxrestaurant_allowedtags['h1'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['h2'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['h3'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['h4'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['h5'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['h6'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['em'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['i'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['li'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['button'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['ul'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['ol'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['pre'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['blockquote'] = array('style' => array());
        $maxrestaurant_allowedtags['<!--more-->'] = array();
        $maxrestaurant_allowedtags['p'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['del'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['span'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['code'] = array('class' => array());
        $maxrestaurant_allowedtags['strong'] = array(
            'class' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['ins'] = array('datetime' => array());
        $maxrestaurant_allowedtags['img'] = array(
            'class' => array(),
            'src' => array(),
            'alt' => array(),
            'style' => array(),
        );
        $maxrestaurant_allowedtags['a'] = array(
            'class' => array(),
            'href' => array(),
            'target' => array(),
            'title' => array(),
            'style' => array(),
        );

        return $maxrestaurant_allowedtags;
    }
endif;


/**
 * Set up the content width value based on the theme's design.
 *
 * @see maxrestaurant_content_width()
 *
 * @since Maxrestaurant 1.0
 */
if (!isset($content_width)) {
    $content_width = 474;
}

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Maxrestaurant 1.0
 */
if (!function_exists('maxrestaurant_content_width')) :

    function maxrestaurant_content_width()
    {
        if (is_attachment() && wp_attachment_is_image()) {
            $GLOBALS['content_width'] = 810;
        }
    }

    add_action('template_redirect', 'maxrestaurant_content_width');
endif;


/**
 * Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Maxrestaurant 1.0
 */
if (!function_exists('maxrestaurant_theme_setup')) :

    function maxrestaurant_theme_setup()
    {

        /* load theme languages */
        load_theme_textdomain('maxrestaurant', get_template_directory() . '/languages');

        /* Image Sizes */
        set_post_thumbnail_size(851, 350, true); /* Default Featured Image */

        add_image_size('maxrestaurant_390_280', 390, 280, true); /* Event Gallery  */

        add_image_size('maxrestaurant_360_532', 360, 532, true); /* Event Single Post Image  */

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(
            array(
                'maxrestaurant_primary_nav' => esc_html__('Primary menu', 'maxrestaurant'),
            )
        );

        add_theme_support('automatic-feed-links');

        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');

        /* WooCommerce Theme Support */
        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-zoom');
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-slider');

        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array('video', 'gallery', 'audio'));
    }

    add_action('after_setup_theme', 'maxrestaurant_theme_setup');
endif;


/* Google Font */
if (!function_exists('maxrestaurant_fonts_url')) :

    function maxrestaurant_fonts_url()
    {

        $fonts_url = '';

        $raleway = _x('on', 'Raleway font: on or off', 'maxrestaurant');
        $greatvibes = _x('on', 'Great Vibes font: on or off', 'maxrestaurant');
        $lato = _x('on', 'Lato font: on or off', 'maxrestaurant');

        if ('off' !== $raleway || 'off' !== $greatvibes || 'off' !== $lato) {

            $font_families = array();

            if ('off' !== $raleway) {
                $font_families[] = 'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i';
            }
            if ('off' !== $greatvibes) {
                $font_families[] = 'Great Vibes';
            }
            if ('off' !== $lato) {
                $font_families[] = 'Lato:100,100i,300,300i,400,400i,700,700i,900,900i';
            }

            $query_args = array(
                'family' => urlencode(implode('|', $font_families)),
                'subset' => urlencode('latin,latin-ext'),
            );

            $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
        }

        return esc_url_raw($fonts_url);
    }
endif;


/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Maxrestaurant 1.0
 */
if (!function_exists('maxrestaurant_enqueue_scripts')) :

    function maxrestaurant_enqueue_scripts()
    {
        $theme = wp_get_theme('maxrestaurant');
        $version = $theme['Version'];

        // Load the html5 shiv.
        wp_enqueue_script('respond.min', get_template_directory_uri() . '/assets/js/html5/respond.min.js', array(), '3.7.3');
        wp_script_add_data('respond.min', 'conditional', 'lt IE 9');

        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        /* Google Font */
        if (function_exists('maxrestaurant_fonts_url')) :
            wp_enqueue_style('maxrestaurant-fonts', maxrestaurant_fonts_url());
        endif;

        wp_enqueue_style('dashicons');

        /* Lib */
        wp_enqueue_style('maxrestaurant-lib', get_template_directory_uri() . '/assets/css/lib.css');
        wp_enqueue_script('maxrestaurant-lib', get_template_directory_uri() . '/assets/js/lib.js', array('jquery'), $version, true);

        wp_add_inline_script(
            'maxrestaurant-lib', '
			var templateUrl = "' . esc_url(get_template_directory_uri()) . '";
			var WPAjaxUrl = "' . admin_url('admin-ajax.php') . '";
		'
        );

        /* Main Style */
        wp_enqueue_style('maxrestaurant-plugins', get_template_directory_uri() . '/assets/css/plugins.css');
        wp_enqueue_style('maxrestaurant-elements', get_template_directory_uri() . '/assets/css/elements.css');
        wp_enqueue_style('maxrestaurant-wordpress', get_template_directory_uri() . '/assets/css/wordpress.css');
        wp_enqueue_style('maxrestaurant-woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css');

        /* RTL Style */
        if (maxrestaurant_options('opt_rtl_switch') == '1') {
            wp_enqueue_style('maxrestaurant-rtl', get_template_directory_uri() . '/assets/css/rtl.css');
        }
        wp_enqueue_script('maxrestaurant-functions', get_template_directory_uri() . '/assets/js/functions.js', array('jquery'), $version, true);
        wp_enqueue_style('maxrestaurant-stylesheet', get_template_directory_uri() . '/style.css');

        /* Custom CSS */
        $custom_css = '
            @media (min-width: 992px) {
                ' . maxrestaurant_options('custom_css_desktop') . '
            }
            @media (max-width: 991px) {
                ' . maxrestaurant_options('custom_css_tablet') . '
            }
            @media (max-width: 767px) {
                ' . maxrestaurant_options('custom_css_mobile') . '
            }           
        ';
        wp_add_inline_style('maxrestaurant-stylesheet', $custom_css);
    }

    add_action('wp_enqueue_scripts', 'maxrestaurant_enqueue_scripts');
endif;

if(!function_exists('maxrestaurant_dequeue_styles')):
	add_action('wp_dequeue_styles', 'maxrestaurant_dequeue_styles', 999);

	function maxrestaurant_dequeue_styles() {
		// wp_dequeue_script('jquery');
        // wp_dequeue_script('jquery-ui-accordion');
		wp_dequeue_style ('font-awesome');
        wp_dequeue_style ('font-awesome-css-cdn');
        wp_dequeue_style ('event-tickets-rsvp');
        wp_dequeue_style ('TribeEventsWooTickets');

	}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * @since Maxrestaurant 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
if (!function_exists('maxrestaurant_body_classes')) :

    function maxrestaurant_body_classes($classes)
    {

        if (is_singular() && !is_front_page()) {
            $classes[] = 'singular';
        }

        if (is_front_page() && !is_home()) {
            $classes[] = 'front-page';
        }

        if (is_404()) {
            $classes[] = '404-template';
        }

        return $classes;
    }

    add_filter('body_class', 'maxrestaurant_body_classes');

endif;


/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Maxrestaurant 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
if (!function_exists('maxrestaurant_post_classes')) :

    function maxrestaurant_post_classes($classes)
    {
        if (!is_attachment() && has_post_thumbnail()) {
            $classes[] = 'has-post-thumbnail';
        }
        return $classes;
    }

    add_filter('post_class', 'maxrestaurant_post_classes');

endif;


if (class_exists('woocommerce')) {

    /* Change number or products per row to 3 */
    if (!function_exists('maxrestaurant_loop_columns')) :

        add_filter('loop_shop_columns', 'maxrestaurant_loop_columns');

        function maxrestaurant_loop_columns()
        {
            if (maxrestaurant_options('opt_wc_column') != '') {
                $wccolumn = maxrestaurant_options('opt_wc_column');
            } else {
                $wccolumn = 3;
            }
            return $wccolumn; // products per row
        }
    endif;

    if (!function_exists('maxrestaurant_related_products_args')) :

        add_filter('woocommerce_output_related_products_args', 'maxrestaurant_related_products_args');

        function maxrestaurant_related_products_args($args)
        {

            $args['posts_per_page'] = 3; // 4 related products
            $args['columns'] = 3; // arranged in 3 columns
            return $args;
        }
    endif;
}

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

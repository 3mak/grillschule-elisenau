<?php
$addclasswidgetclass = '';
if ( maxrestaurant_options( 'opt_footer_widcol' ) == '1col' ) {
	$addclasswidgetclass = 'col-md-12 col-sm-12 col-xs-12 one_col';
} elseif ( maxrestaurant_options( 'opt_footer_widcol' ) == '2col' ) {
	$addclasswidgetclass = 'col-md-6 col-sm-6 col-xs-6 two_col';
} elseif ( maxrestaurant_options( 'opt_footer_widcol' ) == '3col' ) {
	$addclasswidgetclass = 'col-md-4 col-sm-6 col-xs-6 three_col';
} else {
	$addclasswidgetclass = 'col-md-3 col-sm-6 col-xs-6 four_col';
}

$widgetarea1 = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_footer_widget_area1', true );
$widgetarea2 = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_footer_widget_area2', true );
$widgetarea3 = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_footer_widget_area3', true );
$widgetarea4 = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_footer_widget_area4', true );
?>
<!-- Footer Main -->
<footer id="footer-main" class="container-fluid no-left-padding no-right-padding footer-main footer-main-2">
	<?php
	if ( is_active_sidebar( 'sidebar-5' ) ||
	   is_active_sidebar( 'sidebar-6' ) ||
	   is_active_sidebar( 'sidebar-7' ) ||
	   is_active_sidebar( 'sidebar-8' ) ||
	   is_active_sidebar( 'sidebar-9' ) ||
	   is_active_sidebar( 'sidebar-10' ) ||
	   is_active_sidebar( 'sidebar-11' ) ||
	   is_active_sidebar( 'sidebar-12' )
	) {
		?>
		<!-- Top Footer -->
		<div class="container-fluid no-left-padding no-right-padding top-footer">
			<!-- Container -->
			<div class="container">
				<!-- Row -->
				<div class="row">
					<?php
					if ( $widgetarea1 != '' && is_active_sidebar( $widgetarea1 ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( $widgetarea1 ); ?>
						</div>
						<?php
					} elseif ( is_active_sidebar( 'sidebar-9' ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( 'sidebar-9' ); ?>
						</div>
						<?php
					}

					if ( $widgetarea2 != '' && is_active_sidebar( $widgetarea2 ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( $widgetarea2 ); ?>
						</div>
						<?php
					} elseif ( is_active_sidebar( 'sidebar-10' ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( 'sidebar-10' ); ?>
						</div>
						<?php
					}

					if ( $widgetarea3 != '' && is_active_sidebar( $widgetarea3 ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( $widgetarea3 ); ?>
						</div>
						<?php
					} elseif ( is_active_sidebar( 'sidebar-11' ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( 'sidebar-11' ); ?>
						</div>
						<?php
					}

					if ( $widgetarea4 != '' && is_active_sidebar( $widgetarea4 ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( $widgetarea4 ); ?>
						</div>
						<?php
					} elseif ( is_active_sidebar( 'sidebar-12' ) ) {
						?>
						<div class="<?php echo esc_attr( $addclasswidgetclass ); ?>">
							<?php dynamic_sidebar( 'sidebar-12' ); ?>
						</div>
						<?php
					}
					?>
				</div><!-- Row /- -->
			</div><!-- Container /- -->
		</div><!-- Top Footer -->
		<?php
	}
	if ( function_exists( 'maxrestaurant_copyright' ) && maxrestaurant_options( 'opt_footer_copyright' ) != '' ) {
		echo maxrestaurant_copyright();
	} else {
		?>
		<!-- Bottom Footer -->
		<div class="container-fluid bottom-footer">
			<p><?php echo esc_html_e( '&copy; Copyrights 2017 - All Rights Reserved', 'maxrestaurant' ); ?></p>
		</div>
		<?php
	}
	?>
</footer><!-- Footer Main /- -->

<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Maxrestaurant
 * @since Maxrestaurant 1.0
 */
?>
<?php
$event_meta = get_post_custom(get_the_id());
global $event_meta;

$startDate = new DateTime($event_meta['mep_event_start_date'][0]);
$endDate = new DateTime($event_meta['mep_event_end_date'][0]);
?>
<!-- Event Single -->
<div class="event-single-content woocommerce">
    <div class="row">
        <div class="eventcontent-block product">
            <?php
            if (has_post_thumbnail()) {
                ?>
                <div class="col-md-4 col-sm-6 col-xs-6 ">
                    <?php the_post_thumbnail('maxrestaurant_360_532'); ?>
                    <?php
                    if (!empty(get_post_meta(get_the_ID(), 'mep_location_venue', true))) {
                        if ($event_meta['longitude'][0] && $event_meta['latitude'][0]) {
                            ?>
                            <div class="event-map map">
                                <div class="map-canvas-1" id="map-canvas-contact-1"
                                     data-lat="<?php echo $event_meta['latitude'][0] ?>" data-lng="<?php echo $event_meta['longitude'][0] ?> "
                                     data-string="<?php echo get_post_meta(get_the_ID(), 'location_name', true) ?>"
                                     data-marker="<?php echo esc_url(MAXRESTAURANT_IMGURI) . '/marker.png'; ?>"
                                     data-zoom="10"></div>
                            </div>
                            <?php
                        } ?>
                    <address class="event-address">
                        <strong><?php echo get_post_meta(get_the_ID(), 'mep_location_venue', true) ?></strong><br>
		                <?php echo $event_meta['mep_street'][0]; ?><br>
		                <?php echo $event_meta['mep_postcode'][0]; ?> <?php echo $event_meta['mep_city'][0] ?>
                    </address>
                    <?php } ?>
                </div>
                <?php
            }
            ?>

            <div class="col-md-8 col-sm-6 col-xs-6">
                <div class="event-details">

                    <span><i class="fa fa-calendar-o"></i>
                        <strong><?php echo $startDate->format('d.m.Y'); ?></strong>
					</span>
                    <span>
						<i class="fa fa-clock-o"></i> <?php echo $startDate->format('H:i'); ?> Uhr
							- <?php echo $endDate->format('H:i'); ?>
                        Uhr
					</span>
                </div>
                <h1 class="product_title entry-title"><?php the_title() ?></h1>
                <p><span class="price"><?php do_action('mep_event_price'); ?></span>  Preis pro Person</p>
                <div class="legal-price-info">
                    <p class="wc-gzd-additional-info">
                        <span class="wc-gzd-additional-info small-business-info">Kein Mehrwertsteuerausweis, da Kleinunternehmer nach §19 (1) UStG.</span>
                    </p>
                </div>
                <div class="event-excerpt">
                    <?php the_content(); ?>
                </div>


                <?php get_template_part( 'template-parts/event', 'book' ); ?>
            </div>

            <div class="col-xs-12">
            </div>
        </div>
    </div>

    <?php

    $ary_imgs = '';
    $ary_imgs = get_post_meta(get_the_ID(), 'maxrestaurant_cf_gallery', true);
    if (get_post_meta(get_the_ID(), 'maxrestaurant_cf_gallery', true) != '') {
        ?>
        <div class="event-gallery container">
            <div class="row">
                <div class="event_gallery-carousel">
                    <?php
                    foreach ((array)$ary_imgs as $attachment_id => $attachment_url) {
                        ?>
                        <div class="event_item">
                            <?php echo wp_get_attachment_image($attachment_id, 'maxrestaurant_390_280'); ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div><!-- Event Single /- -->
<script>
    jQuery('#quantity_5a7abbd1bff73').click(function () {
        var $form = jQuery('form'); //on a real app it would be better to have a class or ID
        var $totalQuant = jQuery('#quantity_5a7abbd1bff73', $form);
        jQuery('#quantity_5a7abbd1bff73', $form).change(calculateTotal);


        function calculateTotal() {
            var sum = jQuery('#rowtotal').val();
        }

    });


    jQuery(document).ready(function () {


        jQuery("#mep-event-accordion").accordion({
            collapsible: true,
            active: false
        });


        jQuery(document).on("change", ".etp", function () {
            var sum = 0;
            jQuery(".etp").each(function () {
                sum += +jQuery(this).val();
            });
            jQuery("#ttyttl").html(sum);
        });


        jQuery("#ttypelist").change(function () {
            vallllp = jQuery(this).val() + "_";
            var n = vallllp.split('_');
            var price = n[0];
            var ctt = 99;
            if (vallllp != "_") {

                var currentValue = parseInt(ctt);
                jQuery('#rowtotal').val(currentValue += parseFloat(price));
            }
            if (vallllp == "_") {
                jQuery('#eventtp').attr('value', 0);
                jQuery('#eventtp').attr('max', 0);
                jQuery("#ttypeprice_show").html("")
            }


        });


        function updateTotal() {
            var total = 0;
            vallllp = jQuery(this).val() + "_";
            var n = vallllp.split('_');
            var price = n[0];
            total += parseFloat(price);
            jQuery('#rowtotal').val(total);
        }


//Bind the change event
        jQuery(".extra-qty-box").on('change', function () {
            var sum = 0;
            var total = <?php if ($event_meta['_price'][0]) {
                echo $event_meta['_price'][0];
            } else {
                echo 0;
            } ?>;

            jQuery('.price_jq').each(function () {
                var price = jQuery(this);
                var count = price.closest('tr').find('.extra-qty-box');
                sum = (price.html() * count.val());
                total = total + sum;
                // price.closest('tr').find('.cart_total_price').html(sum + "₴");

            });

            jQuery('#rowtotal').val(total);

        }).change(); //trigger change event on page load

        <?php
        $mep_event_ticket_type = get_post_meta($post->ID, 'mep_event_ticket_type', true);
        if($mep_event_ticket_type){
        $count = 1;
        foreach ( $mep_event_ticket_type as $field ) {
        $qm = $field['option_name_t'];
        ?>

        jQuery('.btn-mep-event-cart').hide();

        jQuery('#eventpxtp_<?php echo $count; ?>').on('change', function () {

            var inputs = jQuery("#ttyttl").html() || 0;
            var inputs = jQuery('#eventpxtp_<?php echo $count; ?>').val() || 0;
            var input = parseInt(inputs);
            var children = jQuery('#dadainfo_<?php echo $count; ?> > div').size() || 0;
            //alert(inputs);
            if (inputs == 0) {
                jQuery('.btn-mep-event-cart').hide();
            } else if (inputs > 0) {
                jQuery('.btn-mep-event-cart').show();
            }
            if (input < children) {
                jQuery('#dadainfo_<?php echo $count; ?>').empty();
                children = 0;
            }

            for (var i = children + 1; i <= input; i++) {
                jQuery('#dadainfo_<?php echo $count; ?>').append(
                    jQuery('<div/>')
                        .attr("id", "newDiv" + i)
                        .html("<?php do_action('mep_reg_fields'); ?>")
                );
            }
        });
        <?php
        $count++;
        }
        }else{
        ?>
        jQuery('#quantity_5a7abbd1bff73').on('change', function () {

            var input = jQuery('#quantity_5a7abbd1bff73').val() || 0;
            var children = jQuery('#divParent > div').size() || 0;

            if (input < children) {
                jQuery('#divParent').empty();
                children = 0;
            }

            for (var i = children + 1; i <= input; i++) {
                jQuery('#divParent').append(
                    jQuery('<div/>')
                        .attr("id", "newDiv" + i)
                        .html("<?php do_action('mep_reg_fields'); ?>")
                );
            }


        });
        <?php
        }
        ?>
    });
</script>

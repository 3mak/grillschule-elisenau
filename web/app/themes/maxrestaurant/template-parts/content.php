<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Maxrestaurant
 * @since Maxrestaurant 1.0
 */

$css = '';
if ( ! has_post_thumbnail() ) {
	$css = 'no-post-thumbnail';
}

$css_content = '';
if ( get_the_content() != '' ) {
	$css_content = '';
} else {
	$css_content = ' no-post-content';
}
$post_css = $css . $css_content;

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $post_css ); ?>>
	<?php
	if ( is_sticky() && is_home() && ! is_paged() ) :
?>
 <div class="entry-cover"><span class="sticky-post"><?php esc_html_e( 'Featured', 'maxrestaurant' ); ?></span></div> <?php endif; ?>
	<?php
	if ( ! has_post_thumbnail() || get_post_format() != 'audio' || get_post_format() != 'video' || get_post_format() != 'gallery' ) {
		?>
		<div class="entry-cover">

			<?php get_template_part( 'template-parts/format', 'gallery' ); ?>

			<?php get_template_part( 'template-parts/format', 'video' ); ?>

			<?php get_template_part( 'template-parts/format', 'audio' ); ?>

			<?php
			if ( has_post_thumbnail() && ( get_post_format() != 'audio' && get_post_format() != 'video' && get_post_format() != 'gallery' ) ) {

				if ( is_single() ) {
					the_post_thumbnail( 'full' );
				} else {
					?>
					<a href="<?php the_permalink(); ?>" class="post-thumbnail"><?php the_post_thumbnail( 'full' ); ?></a>
					<?php
				}
			}
			?>
		</div>
	<?php
	}
	?>
	
	<div class="entry-content">
		<div class="post-date">
			<?php
			if ( is_single() ) {
				echo get_the_date( 'j' );
				?>
				<span><?php echo get_the_date( 'M' ); ?></span>
				<?php
			} else {
				?>
				<a href="<?php the_permalink(); ?>"> <?php echo get_the_date( 'j' ); ?><span><?php echo get_the_date( 'F' ); ?></span></a>
				<?php
			}
			?>
		</div>
		<?php
		if ( ! is_single() ) {
			the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
		}
		?>
		<div class="entry-meta">
			<span class="post-date">
				<i class="fa fa-clock-o"></i>
				<?php esc_html_e( 'Posted', 'maxrestaurant' ); ?>
				<?php
				if ( is_single() ) {
					echo get_the_date( 'Y,' );
					echo get_the_time( 'g:i a' );
				} else {
					?>
					<a href="<?php the_permalink(); ?>"><?php echo get_the_date( 'Y,' ); ?><?php echo get_the_time( 'g:i a' ); ?></a>
					<?php
				}
				?>
			</span>
			<?php
			if ( get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_post_location', true ) != '' ) {
				?>
				<span><i class="fa fa-map-marker"></i><?php echo esc_attr( get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_post_location', true ) ); ?></span>
				<?php
			}
			?>
		</div>
		<?php
		if ( is_single() ) {
			/* translators: %s: Name of current post */
			the_content(
				sprintf(
					esc_html__( 'Continue reading %s', 'maxrestaurant' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				)
			);

			wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'maxrestaurant' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'maxrestaurant' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			);
		} else {
			if ( get_the_content() != '' ) {
				echo wpautop( wp_kses( wp_html_excerpt( strip_shortcodes( get_the_content() ), 225, ' [...]' ), maxrestaurant_allowhtmltags() ) );
			}
			?>
			<a href="<?php the_permalink(); ?>" title="<?php esc_html_e( 'Read More', 'maxrestaurant' ); ?>">
			<?php esc_html_e( 'Read More', 'maxrestaurant' ); ?>
			</a>
			<?php
		}
		?>
	</div>
	<?php
	if ( is_single() ) {
		if ( has_tag() && maxrestaurant_options( 'opt_post_tags' ) != '0' ) {
			?>
			<div class="entry-footer">
				<span class="tags-links"><?php esc_html_e( 'Tags:', 'maxrestaurant' ); ?> 
					<?php echo get_the_tag_list( ' ' ); ?>
				</span>
			</div>
			<?php
		}
		if ( maxrestaurant_options( 'opt_post_category' ) != '0' ) {
			?>
			<div class="post-categories">
				<span><?php esc_html_e( 'Categories :', 'maxrestaurant' ); ?></span>
				<?php the_category( '  ' ); ?>
			</div>
			<?php
		}
		if ( maxrestaurant_options( 'opt_post_Share' ) != '0' ) {
			?>
			<div class="social-share-block">
				<span><i class="fa fa-share"></i><?php esc_html_e( ' share:', 'maxrestaurant' ); ?></span>
				<ul class="social-share">
					<li><a href="javascript: void(0)" data-action="facebook" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url( the_permalink() ); ?>"><i class="fa fa-facebook"></i></a></li>
					<li><a href="javascript: void(0)" data-action="twitter" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url( the_permalink() ); ?>"><i class="fa fa-twitter"></i></a></li>
					<li><a href="javascript: void(0)" data-action="google-plus" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url( the_permalink() ); ?>"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="javascript: void(0)" data-action="linkedin" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url( the_permalink() ); ?>"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="javascript: void(0)" data-action="instagram" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url( the_permalink() ); ?>"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
			<?php
		}
		if ( maxrestaurant_options( 'opt_post_author' ) != '0' ) {
			if ( get_the_author_meta( 'description' ) != '' && get_avatar( get_the_author_meta( 'ID' ) != '' ) ) {
				?>
				<div class="about-author">
					<div class="author-details">
						<i><?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?></i>
						<?php echo wpautop( wp_kses( get_the_author_meta( 'description' ), maxrestaurant_allowhtmltags() ) ); ?>
					</div>
				</div>
				<?php
			}
		}
	}
	?>
</article>

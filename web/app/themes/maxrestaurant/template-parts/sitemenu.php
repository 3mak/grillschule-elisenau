<?php
if ( has_nav_menu( 'maxrestaurant_primary_nav' ) ) :
	wp_nav_menu(
		array(
			'theme_location' => 'maxrestaurant_primary_nav',
			'container'      => false,
			'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'          => 10,
			'menu_class'     => 'nav navbar-nav navbar-right',
			'walker'         => new maxrestaurant_nav_walker(),
		)
	);
	else :
	?>
	<h3 class="menu-setting-info">
		<a href="<?php echo esc_url( home_url( '/wp-admin/nav-menus.php' ) ); ?>"><?php esc_html_e( 'Set The Menu', 'maxrestaurant' ); ?></a>
	</h3>
	<?php
endif;
?>

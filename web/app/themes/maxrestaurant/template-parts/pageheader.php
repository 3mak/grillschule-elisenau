<?php
/* Page Metabox */
$page_subtitle = '';
$page_subtitle = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_page_subtitle', true );

$page_banner = '';
$page_banner = get_post_meta( maxrestaurant_get_the_ID(), 'maxrestaurant_cf_page_header_img', true );

/* Page Banner */
$header_image = '';
if ( $page_banner != '' ) {
	$header_image = $page_banner;
} elseif ( maxrestaurant_options( 'opt_pageheader_img', 'url' ) != '' ) {
	$header_image = maxrestaurant_options( 'opt_pageheader_img', 'url' );
} else {
	$header_image = MAXRESTAURANT_IMGURI . '/page-banner.jpg';
}

if ( is_search() ) {
	if ( maxrestaurant_options( 'opt_search_bg', 'url' ) != '' ) {
		$header_image = maxrestaurant_options( 'opt_search_bg', 'url' );
	} else {
		$header_image = MAXRESTAURANT_IMGURI . '/page-banner.jpg';
	}
}

if ( is_404() ) {
	if ( maxrestaurant_options( 'opt_404_bg', 'url' ) != '' ) {
		$header_image = maxrestaurant_options( 'opt_404_bg', 'url' );
	} else {
		$header_image = MAXRESTAURANT_IMGURI . '/page-banner.jpg';
	}
}
?>
	<div class="container-header no-left-padding no-right-padding page-banner text-center custombg_overlay" style="
		background-image: url(<?php echo esc_url( $header_image ); ?>);
		<?php
		if ( maxrestaurant_options( 'opt_pageheader_height' ) != '' ) {
?>
 min-height: <?php echo maxrestaurant_options( 'opt_pageheader_height' ); ?>px;<?php } ?>
		">
		<!-- Container -->
		<div class="container">
			<h3>
			<?php
			if ( is_home() ) {
				esc_html_e( 'Blog', 'maxrestaurant' );
			} elseif ( is_404() ) {
				esc_html_e( 'Page Not Found', 'maxrestaurant' );
			} elseif ( is_search() ) {
				printf( esc_html__( 'Search Results for: %s', 'maxrestaurant' ), get_search_query() );
			} elseif ( is_archive() ) {
				the_archive_title();
			} else {
				the_title();
			}
			if ( $page_subtitle != '' ) {
				?>
				<span class="page-subtitle"><?php echo esc_attr( $page_subtitle ); ?></span>
				<?php
			}
				?>
			</h3>
		</div><!-- Container /- -->
	</div>

<?php
/**
 * Template Name: Maxtoolkit
 * @package WordPress
 * @subpackage Maxrestaurant
 * @since Maxrestaurant 1.0
 */
?>
<?php
$event_meta = get_post_custom(get_the_id());
?>
<?php get_template_part( 'template-parts/event', 'before' ); ?>

<?php
// Start the loop.
while ( have_posts() ) :
	the_post();

	// Include the page content template.
	get_template_part( 'template-parts/content', 'event' );

	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

	// End the loop.
endwhile;
?>

<?php
get_template_part( 'template-parts/event', 'after' );

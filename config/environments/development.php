<?php
/** Development */
define('SAVEQUERIES', true);
define('WP_DEBUG', false);
define('SCRIPT_DEBUG', false);
define('FS_METHOD', 'direct');
define('ALLOW_UNFILTERED_UPLOADS', true);

ini_set('display_errors',1);
error_reporting( E_ALL & ~E_DEPRECATED & ~E_STRICT );
